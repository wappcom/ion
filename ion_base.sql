-- phpMyAdmin SQL Dump
-- version 5.2.1
-- https://www.phpmyadmin.net/
--
-- Servidor: localhost
-- Tiempo de generación: 06-03-2024 a las 10:17:56
-- Versión del servidor: 8.0.36-0ubuntu0.22.04.1
-- Versión de PHP: 8.2.15

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `ion_base`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `blocks`
--

CREATE TABLE `blocks` (
  `block_id` int NOT NULL,
  `block_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `block_class` varchar(44) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `block_order` int DEFAULT NULL,
  `block_parent_id` int NOT NULL,
  `block_ent_id` int NOT NULL DEFAULT '0',
  `block_state` int NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Volcado de datos para la tabla `blocks`
--

INSERT INTO `blocks` (`block_id`, `block_name`, `block_class`, `block_order`, `block_parent_id`, `block_ent_id`, `block_state`) VALUES
(1, 'Header', 'headerPage', 1, 0, 1, 1),
(2, 'Body', 'bodyPage', 2, 0, 1, 1),
(3, 'Footer', 'footerPage', 3, 0, 1, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `categorys`
--

CREATE TABLE `categorys` (
  `cat_id` int NOT NULL,
  `cat_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `cat_path` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `cat_pathurl` varchar(1000) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `cat_description` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci,
  `cat_order` int DEFAULT NULL,
  `cat_parent_id` int DEFAULT '0',
  `cat_related` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `cat_icon` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `cat_color` varchar(11) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `cat_img` int DEFAULT NULL,
  `cat_banner` int DEFAULT NULL,
  `cat_ent_id` int DEFAULT NULL,
  `cat_configpath` varchar(400) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `cat_cls` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `cat_json` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci,
  `cat_state_collapse` int NOT NULL DEFAULT '1',
  `cat_state` int NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Volcado de datos para la tabla `categorys`
--

INSERT INTO `categorys` (`cat_id`, `cat_name`, `cat_path`, `cat_pathurl`, `cat_description`, `cat_order`, `cat_parent_id`, `cat_related`, `cat_icon`, `cat_color`, `cat_img`, `cat_banner`, `cat_ent_id`, `cat_configpath`, `cat_cls`, `cat_json`, `cat_state_collapse`, `cat_state`) VALUES
(16, 'home', 'home', '/home', '', 0, 0, '', '', '', 0, 0, 1, '', '', '', 1, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `categorys_files`
--

CREATE TABLE `categorys_files` (
  `cat_file_file_id` int NOT NULL,
  `cat_file_cat_id` int NOT NULL,
  `cat_file_order` int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `contents`
--

CREATE TABLE `contents` (
  `cont_id` int NOT NULL,
  `cont_title` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `cont_pathurl` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `cont_summary` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci,
  `cont_tags` varchar(300) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `cont_img` int DEFAULT NULL,
  `cont_body` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci,
  `cont_author` int DEFAULT NULL,
  `cont_register_date` datetime DEFAULT NULL,
  `cont_user_id` int DEFAULT NULL COMMENT 'user register',
  `cont_notitle` int DEFAULT '0',
  `cont_url` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `cont_btn_title` varchar(45) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `cont_icon` varchar(21) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `cont_target` varchar(12) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '_blank',
  `cont_cls` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT 'css class',
  `cont_json` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci,
  `cont_state` int NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `contents_categorys`
--

CREATE TABLE `contents_categorys` (
  `cont_cat_cont_id` int NOT NULL,
  `cont_cat_cat_id` int NOT NULL,
  `cont_cat_ent_id` int DEFAULT NULL,
  `cont_cat_order` int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `contents_files`
--

CREATE TABLE `contents_files` (
  `cont_file_cont_id` int NOT NULL,
  `cont_file_file_id` int NOT NULL,
  `cont_file_title` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `cont_file_summary` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci,
  `cont_file_url` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `cont_file_btn_title` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `cont_file_icon` varchar(25) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `cont_file_target` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '_blank',
  `cont_file_order` int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `contents_pubs`
--

CREATE TABLE `contents_pubs` (
  `cont_pub_cont_id` int NOT NULL,
  `cont_pub_pub_id` int NOT NULL,
  `cont_pub_order` int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `docs`
--

CREATE TABLE `docs` (
  `doc_id` int NOT NULL,
  `doc_file_id` int DEFAULT NULL,
  `doc_title` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `doc_description` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci,
  `doc_img` int NOT NULL DEFAULT '0' COMMENT 'customized',
  `doc_date` datetime DEFAULT NULL,
  `doc_register_date` datetime DEFAULT NULL,
  `doc_user_id` int NOT NULL DEFAULT '0',
  `doc_json` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci,
  `doc_ent_id` int DEFAULT NULL,
  `doc_state` int NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `docs_categorys`
--

CREATE TABLE `docs_categorys` (
  `doc_cat_doc_id` int NOT NULL,
  `doc_cat_cat_id` int NOT NULL,
  `doc_cat_order` int NOT NULL,
  `doc_cat_ent_id` int DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `entities`
--

CREATE TABLE `entities` (
  `ent_id` int NOT NULL,
  `ent_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `ent_path` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `ent_brand` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `ent_code` varchar(11) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `ent_token` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `ent_record_date` datetime DEFAULT NULL,
  `ent_type` int DEFAULT NULL,
  `ent_notes` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `ent_state` int DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Volcado de datos para la tabla `entities`
--

INSERT INTO `entities` (`ent_id`, `ent_name`, `ent_path`, `ent_brand`, `ent_code`, `ent_token`, `ent_record_date`, `ent_type`, `ent_notes`, `ent_state`) VALUES
(1, 'Wappcom', 'wappcom', 'modules/assets/img/logo.svg', 'CO1', 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiI3IiwibmFtZSI6IkNhc2EgZGUgT3JhY2lvbiIsImlhdCI6MTIzNDU2Nzg5fQ.y1fa_y48leQNYbZGDlvJewtehVj78zMhkJ73B-8MGyE', '2020-10-20 14:55:39', 1, '', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `files`
--

CREATE TABLE `files` (
  `file_id` int NOT NULL,
  `file_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `file_description` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `file_pathurl` varchar(1000) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `file_embed` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `file_title` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `file_btn_title` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `file_url` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `file_target` varchar(12) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT '_self',
  `file_ext` varchar(11) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `file_alt` varchar(250) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `file_filename_md5` binary(255) DEFAULT NULL,
  `file_datatime` datetime DEFAULT NULL,
  `file_ent_id` int DEFAULT NULL,
  `file_state` int DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `groups`
--

CREATE TABLE `groups` (
  `group_id` int NOT NULL,
  `group_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `group_description` tinytext CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci,
  `group_state` int DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `groups_files`
--

CREATE TABLE `groups_files` (
  `group_file_group_id` int NOT NULL,
  `group_file_file_id` int NOT NULL,
  `group_file_order` int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `links`
--

CREATE TABLE `links` (
  `lnk_id` int NOT NULL,
  `lnk_title` varchar(455) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `lnk_description` varchar(445) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `lnk_tags` varchar(455) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `lnk_url` varchar(455) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT '#',
  `lnk_target` varchar(12) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT '_blank',
  `lnk_icon` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `lnk_attr` varchar(120) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `lnk_cls` varchar(45) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `lnk_alt` varchar(120) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `lnk_img` int DEFAULT NULL,
  `lnk_json` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci,
  `lnk_ent_id` int DEFAULT NULL,
  `lnk_state` int NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `links_categorys`
--

CREATE TABLE `links_categorys` (
  `lnk_cat_cat_id` int NOT NULL,
  `lnk_cat_lnk_id` int NOT NULL,
  `lnk_cat_order` int DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `links_pubs`
--

CREATE TABLE `links_pubs` (
  `lnk_pub_pub_id` int NOT NULL,
  `lnk_pub_lnk_id` int NOT NULL,
  `lnk_pub_order` int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `menus`
--

CREATE TABLE `menus` (
  `menu_id` int NOT NULL,
  `menu_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `menu_description` tinytext CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci,
  `menu_ent_id` int DEFAULT NULL,
  `menu_state` int NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `menus_items`
--

CREATE TABLE `menus_items` (
  `menu_item_id` int NOT NULL,
  `menu_item_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `menu_item_description` tinytext CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci,
  `menu_item_menu_id` int DEFAULT NULL,
  `menu_item_cat_id` int DEFAULT NULL,
  `menu_item_cat_active` int DEFAULT '0',
  `menu_item_pathurl` varchar(400) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `menu_item_target` varchar(12) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '_self',
  `menu_item_parent_id` int DEFAULT '0',
  `menu_item_icon` int DEFAULT NULL,
  `menu_item_img` int DEFAULT NULL,
  `menu_item_order` int DEFAULT NULL,
  `menu_item_state` int NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `modules`
--

CREATE TABLE `modules` (
  `mod_id` int NOT NULL,
  `mod_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `mod_description` tinytext CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci,
  `mod_pathurl` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `mod_path` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `mod_code` varchar(11) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `mod_icon` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `mod_color` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `mod_parent_id` int DEFAULT NULL,
  `mod_indexjs` varchar(400) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `mod_css` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `mod_db` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT 'database',
  `mod_prefix_db` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `mod_relations_db` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `mod_state` int NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Volcado de datos para la tabla `modules`
--

INSERT INTO `modules` (`mod_id`, `mod_name`, `mod_description`, `mod_pathurl`, `mod_path`, `mod_code`, `mod_icon`, `mod_color`, `mod_parent_id`, `mod_indexjs`, `mod_css`, `mod_db`, `mod_prefix_db`, `mod_relations_db`, `mod_state`) VALUES
(1, 'systems', NULL, 'systems', 'modules/systems/', 'SYS1', 'icon icon-systems', '#99C14C', 0, 'components/sistems.js', 'assets/css/dist/systems.css', 'systems', 'sys_', '', 0),
(2, 'modules', NULL, 'modules', 'modules/modules/', 'MOD1', 'icon icon-box-close', '#99C14C', 0, 'components/modules.js', 'assets/css/dist/modules.css', 'modules', 'mod_', 'modules_categorys:mod_cat_mod_id,systems_modules:sys_mod_mod_id', 0),
(3, 'sites', NULL, 'sites', 'modules/websites/', 'SIT1', 'icon icon-blocks', '#8900ff', 0, 'components/sites.js', 'assets/css/dist/sites.css', 'sites', 'site_', '', 0),
(4, 'Hojas de Trabajo', NULL, 'worksheets', 'modules/websites/', 'WS1', 'icon icon-worksheets', '#26fff6', 0, 'components/worksheets.js', 'assets/css/dist/worksheets.css', 'worksheets', 'ws_', '', 1),
(5, 'Bloques', NULL, 'blocks', 'modules/websites/', 'BL1', 'icon icon-blocks-web', '#FEBF10', 0, 'components/blocks.js', 'assets/css/dist/blocks.css', 'blocks', 'block_', '', 1),
(6, 'Publicaciones', NULL, 'publications', 'modules/websites/', 'PUB1', 'icon icon-webpart', '#FEBF10', 0, 'components/publications.js', 'assets/css/dist/publications.css', 'publications', 'pub_', '', 1),
(10, 'Dashboard WebSites', NULL, 'websites', 'modules/websites/', 'WEB1', 'icon icon-dashboard', '#99C14C', 0, 'components/websites.js', 'assets/css/dist/websites.min.css', '', '', '', 1),
(11, 'Contenidos', NULL, 'contents', 'modules/websites/', 'CON1', 'icon icon-content', '#1ff4ed', 0, 'components/contents.js', 'assets/css/dist/contents.min.css', 'contents', 'cont_', 'contents_categorys:cont_cat_cont_id,contents_files:cont_file_cont_id', 1),
(12, 'Categorias', NULL, 'categorys', 'modules/websites/', 'CAT1', 'icon icon-category', '#dd4b4d', 0, 'components/categorys.js', 'assets/css/dist/categorys.min.css', '', '', '', 1),
(13, 'Media', NULL, 'media', 'modules/websites/', 'MED1', 'icon icon-media', '#f04f04', 0, 'components/media.js', 'assets/css/dist/media.min.css', '', '', '', 1),
(14, 'Documentos', NULL, 'docs', 'modules/websites/', 'DOC1', 'icon icon-folder', '#FEBF10', 0, 'components/docs.js', 'assets/css/dist/docs.min.css', '', '', '', 1),
(15, 'Posts', NULL, 'posts', 'modules/websites/', 'POST1', 'icon icon-news', '#37ff2c', 0, 'components/posts.js', 'assets/css/dist/posts.min.css', '', '', '', 1),
(16, 'Enlaces', NULL, 'links', 'modules/websites/', 'LINK1', 'icon icon-link', '#26fff6', 0, 'components/links.js', 'assets/css/dist/links.min.css', '', '', '', 1),
(17, 'Sliders', NULL, 'sliders', 'modules/websites/', 'SLD1', 'icon icon-slider', '#8900ff', 0, 'components/sliders.js', 'assets/css/dist/sliders.min.css', '', '', '', 1),
(18, 'Navegación', NULL, 'navigation', 'modules/websites/', 'NV1', 'icon icon-compass', '#8933ff', 0, 'components/navigation.js', 'assets/css/dist/navigation.min.css', '', '', '', 1),
(200, 'Dashboard RRHH', NULL, 'rrhh', 'modules/rrhh/', 'RRHH', 'icon icon-category', '#99C14C', 0, 'components/rrhh.js', 'assets/css/dist/rrhh.min.css', '', '', '', 0),
(210, 'kardex', NULL, 'kardex', 'modules/rrhh/', 'KDX', 'icon icon-users', '#FEBF10', 0, 'components/kardex.js', '', '', '', '', 0),
(220, 'Organigrama', NULL, 'organizationChart', 'modules/rrhh/', 'KDX', 'icon icon-users', '#FEBF10', 0, 'components/organizationChart.js', 'assets/css/dist/organizationChart', '', '', '', 0),
(400, 'Dashboard Inventario', NULL, 'inventory', 'modules/inventory/', 'INV', 'icon icon-dashboard', '#FEBF10', 0, 'components/inventory.js', 'assets/css/dist/inventory.css', '', '', '', 1),
(401, 'Productos', NULL, 'products', 'modules/inventory/', 'PRO', 'icon icon-box', '#CCC', 0, 'components/products.js', 'assets/css/dist/products.css', 'mod_products', 'mod_prod_', '', 1),
(402, 'Stock', NULL, 'stock', 'modules/inventory/', 'STC', 'icon icon-unchecked', '#99C14C', 0, 'components/stock.js', 'assets/css/dist/stock.css', '', '', '', 1),
(403, 'Lista de Precios', NULL, 'pricelist', 'modules/inventory/', 'STC', 'icon icon-coin', '#99C14C', 0, 'components/pricelist.js', 'assets/css/dist/pricelist.css', '', '', '', 1),
(500, 'Dashboard Contabilidad', NULL, 'accounting', 'modules/accounting/', 'ACC', 'icon icon-dashboard', '#99C14C', 0, 'components/accounting.js', 'assets/css/dist/accounting.css', '', '', '', 1),
(600, 'Dashboard Consejeria', NULL, 'counseling', 'modules/counseling/', NULL, 'icon icon-dashboard', '#99C14C', 0, 'components/counseling.js', '', '', '', '', 1),
(601, 'Aconsejados', NULL, 'advised', 'modules/counseling/', NULL, 'icon icon-users', '#FEBF10', 0, 'components/advised.js', '', '', '', '', 1),
(602, 'Agenda', NULL, 'calendarAdvised', 'modules/counseling/', NULL, 'icon icon-calendar', '#FEBF10', 0, 'components/calendarAdvised.js', 'assets/css/dist/calendarAdvised.css', '', '', '', 1),
(603, 'Reportes', NULL, 'reportsCounseling', 'modules/counseling/', NULL, 'icon icon-table-check', '#FEBF10', 0, 'components/reportCounseling.js', 'assets/css/dist/reportCounseling.css', '', '', '', 1),
(700, 'Dashboard Ventas', NULL, 'sales', 'modules/sales/', 'PVD', 'icon icon-dashboard', '#fcc54e', 0, 'components/sales.js', 'assets/css/dist/sales.css', '', '', '', 1),
(701, 'Punto de Venta Tickets', NULL, 'salesPointTickets', 'modules/sales/', 'PVT', 'icon icon-cash-register', '#fcc54e', 0, 'components/salesPointTickets.js', 'assets/css/dist/salesPointTickets.css', '', '', '', 1),
(800, 'Dashboard Brokers', NULL, 'brokers', 'modules/brokers/', 'PV', 'icon icon-dashboard', '#fcc54e', 0, 'components/brokers.js', 'assets/css/dist/brokers.css', '', '', '', 1),
(801, 'Clientes', NULL, 'brokersCustomers', 'modules/brokers/', 'PV', 'icon icon-users', '#fcc54e', 0, 'components/brokersCustomers.js', 'assets/css/dist/brokersCustomers.css', '', '', '', 1),
(1000, 'Dashboard Suscripciones', NULL, 'suscriptions', 'modules/accounts/', 'SC1', 'icon icon-dashboard', '#99C14', 0, 'components/subscriptions.js', 'assets/css/dist/subscriptions.css', '', '', '', 1),
(1001, 'Cuentas de Usuarios', NULL, 'accountsUsers', 'modules/accounts/', 'ACC1', 'icon icon-user', '#8900ff', 0, 'components/accountsUsers.js', 'assets/css/dist/subscriptions.css', '', '', '', 1),
(1002, 'Cuentas de Clientes', NULL, 'customers', 'modules/accounts/', 'CS1', 'icon icon-folder-open', '#07c472', 0, 'components/customers.js', 'assets/css/dist/customers.css', '', '', '', 1),
(1200, 'Dashboard FLS', NULL, 'fls', 'modules/fls/', 'FLS', 'icon icon-dashboard', '#fcc54e', 0, 'components/fls.js', 'assets/css/dist/fls.css', '', '', '', 1),
(1201, 'Clientes', NULL, 'flsCustomers', 'modules/fls/', 'PV', 'icon icon-users', '#fcc54e', 0, 'components/flsCustomers.js', 'assets/css/dist/flsCustomers.css', '', '', '', 1),
(1300, 'Dashboard Boletería', NULL, 'tickets', 'modules/tickets/', 'TKS', 'icon icon-dashboard', '#fcc54e', 0, 'components/tickets.js', 'assets/css/dist/tickets.min.css', '', '', '', 1),
(1301, 'Eventos', NULL, 'ticketingEvents', 'modules/tickets/', 'EVTKS', 'icon icon-tag', '#fcc54e', 0, 'components/ticketingEvents.js', 'assets/css/dist/ticketingEvents.min.css', 'mod_events', 'mod_eve_', '', 1),
(1400, 'Dashboard Rehabilitación', NULL, 'rehabilitation', 'modules/rehabilitation/', 'RHB', 'icon icon-dashboard', '#fcc54e', 0, 'components/rehabilitation.js', 'assets/css/dist/rehabilitation.min.css', '', '', '', 1),
(1401, 'Registro', NULL, 'rehabilitationRegistry', 'modules/rehabilitation/', 'RHBR', 'icon icon-user', '#fcc54e', 0, 'components/rehabilitationRegistry.js', 'assets/css/dist/rehabilitationRegistry.min.css', '', '', '', 1),
(1500, 'Dashboard Ads', NULL, 'ads', 'modules/ads/', 'ADS1', 'icon icon-dashboard', '#8900ff', 0, 'components/ads.js', 'assets/css/dist/ads.css', '', '', '', 1),
(1501, 'Lugares', NULL, 'places', 'modules/ads/', 'ADS1', 'icon icon-pointer', '#07c472', 0, 'components/places.js', 'assets/css/dist/places.min.css', '', '', '', 0),
(1502, 'Display', NULL, 'displayAds', 'modules/ads/', 'ADS1', 'icon icon-copy', '#07c472', 0, 'components/displayAds.js', 'assets/css/dist/displayAds.min.css', '', '', '', 1),
(1600, 'Dashboard LMS', NULL, 'lms', 'modules/lms/', 'ADS1', 'icon icon-dashboard', '#8900ff', 0, 'components/lms.js', 'assets/css/dist/lms.min.css', '', '', '', 1),
(1601, 'Plan de Estudios', NULL, 'syllabus', 'modules/lms/', 'SB1', 'icon icon-category-r', '#8900ff', 0, 'components/syllabus.js', 'assets/css/dist/syllabus.min.css', '', '', '', 1),
(1700, 'Dashboard Activo Fijo', NULL, 'fixedassets', 'modules/fixedassets/', 'AF1', 'icon icon-dashboard', '#8900ff', 0, 'components/fixedassets.js', 'assets/css/dist/fixedassets.min.css', '', '', '', 1),
(1701, 'Activos', NULL, 'assets', 'modules/fixedassets/', 'ASS1', 'icon icon-tag-qr', '#8900ff', 0, 'components/assets.js', 'assets/css/dist/assets.min.css', '', '', '', 1),
(1800, 'Dashboard Operaciones', NULL, 'operations', 'modules/operations/', 'OP1', 'icon icon-dashboard', '#8900ff', 0, 'components/operations.js', 'assets/css/dist/operations.min.css', '', '', '', 1),
(1900, 'Dashboard CRM', NULL, 'crm', 'modules/crm/', 'CRM1', 'icon icon-dashboard', '#8900ff', 0, 'components/crm.js', 'assets/css/dist/crm.min.css', '', '', '', 1),
(1901, 'Clientes', NULL, 'customersCrm', 'modules/crm/', 'CS1', 'icon icon-folder-open', '#8900ff', 0, 'components/customersCrm.js', 'assets/css/dist/customersCrm.min.css', '', '', '', 1),
(2000, 'Dashboard', NULL, 'restaurants', 'modules/restaurants/', 'RST1', 'icon icon-dashboard', '#8900ff', 0, 'components/restaurants.js', 'assets/css/dist/restaurants.min.css', '', '', '', 1),
(2001, 'Ordenes', NULL, 'commands', 'modules/restaurants/', 'CS1', 'icon icon-command', '#8900ff', 0, 'components/commands.js', 'assets/css/dist/commands.min.css', '', '', '', 1),
(2100, 'Dashboard', NULL, 'externaldocuments', 'modules/externaldocuments/', 'EXD1', 'icon icon-dashboard', '#8900ff', 0, 'components/externaldocuments.js', 'assets/css/dist/externaldocuments.min.css', '', '', '', 1),
(2101, 'Bandeja de Entrada', NULL, 'inboxEd', 'modules/externaldocuments/', 'IBX1', 'icon icon-inbox', '#8900ff', 0, 'components/inboxEd.js', 'assets/css/dist/inboxEd.min.css', '', '', '', 1),
(2102, 'Plantillas', NULL, 'templatesEd', 'modules/externaldocuments/', 'TMP-ED1', 'icon icon-news', '#ff9000', 0, 'components/templatesEd.js', 'assets/css/dist/templatesEd.min.css', '', '', '', 1),
(2103, 'Traking', NULL, 'trakingEd', 'modules/externaldocuments/', 'TRK-ED1', 'icon icon-pointer', '#ff9040', 0, 'components/trakingEd.js', 'assets/css/dist/trakingEd.min.css', '', '', '', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `modules_categorys`
--

CREATE TABLE `modules_categorys` (
  `mod_cat_mod_id` int NOT NULL,
  `mod_cat_cat_id` int NOT NULL,
  `mod_cat_ent_id` int NOT NULL,
  `mod_cat_order` int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `mod_accounting_plan`
--

CREATE TABLE `mod_accounting_plan` (
  `mod_acp_id` int NOT NULL,
  `mod_acp_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `mod_acp_descripcion` varchar(450) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `mod_acp_parent_id` int DEFAULT NULL,
  `mod_acp_code` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `mod_acp_level` int DEFAULT NULL,
  `mod_acp_coin` varchar(5) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `mod_acp_state` int NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `mod_accounts_addresses`
--

CREATE TABLE `mod_accounts_addresses` (
  `mod_acu_add_acu_id` int NOT NULL,
  `mod_acu_add_add_id` int NOT NULL,
  `mod_acu_add_order` int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `mod_accounts_ads`
--

CREATE TABLE `mod_accounts_ads` (
  `mod_aca_id` int NOT NULL,
  `mod_aca_cen_id` int NOT NULL,
  `mod_aca_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `mod_aca_description` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci,
  `mod_aca_code` int DEFAULT NULL,
  `mod_aca_ent_id` int DEFAULT NULL,
  `mod_aca_state` int DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `mod_accounts_invoices_data`
--

CREATE TABLE `mod_accounts_invoices_data` (
  `mod_acu_invd_id` int NOT NULL,
  `mod_acu_invd_acu_id` int NOT NULL,
  `mod_acu_invd_razon_social` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `mod_acu_invd_nit` int NOT NULL,
  `mod_acu_invd_order` int NOT NULL,
  `mod_acu_invd_state` int DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `mod_accounts_roles`
--

CREATE TABLE `mod_accounts_roles` (
  `mod_acu_rol_id` int NOT NULL,
  `mod_acu_rol_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `mod_acu_rol_description` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `mod_acu_rol_parent_id` int DEFAULT NULL,
  `mod_acu_rol_redirection_url` varchar(1000) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `mod_acu_rol_json` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci,
  `mod_acu_rol_state` int DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `mod_accounts_services`
--

CREATE TABLE `mod_accounts_services` (
  `mod_acu_sv_sv_id` int NOT NULL,
  `mod_acu_sv_acu_id` int NOT NULL,
  `mod_acu_sv_order` int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `mod_accounts_token`
--

CREATE TABLE `mod_accounts_token` (
  `mod_atk_user_id` bigint NOT NULL,
  `mod_atk_type` varchar(15) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `mod_atk_token` varchar(400) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `mod_atk_expires_in` datetime NOT NULL,
  `mod_atk_date` datetime NOT NULL,
  `mod_atk_dates_browser` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `mod_accounts_users`
--

CREATE TABLE `mod_accounts_users` (
  `mod_acu_id` int NOT NULL,
  `mod_acu_name` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `mod_acu_fathers_lastname` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `mod_acu_mothers_lastname` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `mod_acu_age_range` varchar(11) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `mod_acu_json` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci,
  `mod_acu_email` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `mod_acu_birthday_date` date DEFAULT NULL,
  `mod_acu_password` varchar(120) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `mod_acu_imagen` varchar(400) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `mod_acu_level` int DEFAULT NULL,
  `mod_acu_gender` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT 'no-gender,male,female',
  `mod_acu_ci` int DEFAULT NULL,
  `mod_acu_ci_ext` varchar(2) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `mod_acu_dial` varchar(5) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `mod_acu_celular` int DEFAULT NULL,
  `mod_acu_record_date` datetime DEFAULT NULL,
  `mod_acu_type` varchar(44) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `mod_acu_city` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `mod_acu_timezone` varchar(120) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `mod_acu_locale` varchar(120) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `mod_acu_token` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `mod_acu_code` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `mod_acu_referred` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `mod_acu_ent_id` int DEFAULT NULL,
  `mod_acu_state` int NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `mod_accounts_users_roles`
--

CREATE TABLE `mod_accounts_users_roles` (
  `mod_acu_user_rol_acu_id` int NOT NULL,
  `mod_acu_user_rol_rol_id` int NOT NULL,
  `mod_acu_user_rol_ent_id` int NOT NULL,
  `mod_acu_user_rol_order` int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `mod_actions`
--

CREATE TABLE `mod_actions` (
  `mod_act_id` int NOT NULL,
  `mod_act_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `mod_act_summary` varchar(455) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `mod_act_path` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `mod_act_state` int NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `mod_actions_register`
--

CREATE TABLE `mod_actions_register` (
  `mod_act_reg_act_id` int NOT NULL COMMENT 'mod_actions',
  `mod_act_reg_cpa_id` int NOT NULL COMMENT 'mod_campaigns_ads',
  `mod_act_reg_aca_id` int NOT NULL COMMENT 'mod_accounts_ads',
  `mod_act_reg_acu_id` int NOT NULL COMMENT 'mod_accounts_user',
  `mod_act_reg_plc_id` int NOT NULL COMMENT 'mod_places',
  `mod_act_reg_value` int NOT NULL,
  `mod_act_reg_register_date` datetime NOT NULL,
  `mod_act_reg_state` int NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `mod_acuse`
--

CREATE TABLE `mod_acuse` (
  `mod_ac_id` int NOT NULL,
  `mod_ac_date` datetime NOT NULL,
  `mod_ac_email` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `mod_addresses`
--

CREATE TABLE `mod_addresses` (
  `mod_add_id` int NOT NULL,
  `mod_add_name` varchar(450) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `mod_add_alias` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `mod_add_description` varchar(700) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `mod_add_coord` varchar(700) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `mod_add_state` int NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `mod_addresses_logistics_distribution_zone`
--

CREATE TABLE `mod_addresses_logistics_distribution_zone` (
  `mod_add_ldz_add_id` int NOT NULL,
  `mod_add_ldz_ldz_id` int NOT NULL,
  `mod_add_ldz_order` int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `mod_ads`
--

CREATE TABLE `mod_ads` (
  `mod_ads_id` int NOT NULL,
  `mod_ads_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `mod_ads_details` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci,
  `mod_ads_code` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `mod_ads_file` int DEFAULT NULL,
  `mod_ads_position` int DEFAULT NULL,
  `mod_ads_user_id` int DEFAULT NULL,
  `mod_ads_state` int NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `mod_ads_customers`
--

CREATE TABLE `mod_ads_customers` (
  `mod_ads_cus_id` int NOT NULL,
  `mod_ads_cus_ads_id` int NOT NULL COMMENT 'mod_ads',
  `mod_ads_cus_cen_id` int NOT NULL COMMENT 'mod_customers_enterprise',
  `mod_ads_cus_date_init` datetime NOT NULL,
  `mod_ads_cus_date_end` timestamp NOT NULL,
  `mod_ads_cus_description` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `mod_ads_cus_state` int NOT NULL DEFAULT '0' COMMENT '0: Inactivo, 1: Activo, 2: Desactivado por tiempo, 3: Desactivado por cantidad de clicks, 4: Desactivado por cantidad de impresiones'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `mod_ads_position`
--

CREATE TABLE `mod_ads_position` (
  `mod_ads_pos_id` int NOT NULL,
  `mod_ads_pos_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `mod_ads_pos_description` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci,
  `mod_ads_pos_state` int NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `mod_advised_calendars`
--

CREATE TABLE `mod_advised_calendars` (
  `mod_adv_cal_id` int NOT NULL,
  `mod_adv_cal_acu_id` int DEFAULT NULL,
  `mod_adv_cal_cou_id` int DEFAULT NULL,
  `mod_adv_cal_type` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT 'appointment',
  `mod_adv_cal_date_start` datetime DEFAULT NULL,
  `mod_adv_cal_date_end` datetime DEFAULT NULL,
  `mod_adv_cal_title` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `mod_adv_cal_details` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci,
  `mod_adv_cal_place` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `mod_adv_cal_reason_cancel` varchar(150) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `mod_adv_cal_reschedule` int DEFAULT NULL,
  `mod_adv_cal_flags` varchar(45) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `mod_adv_cal_ent_id` int DEFAULT NULL,
  `mod_adv_cal_user_id` int DEFAULT NULL,
  `mod_adv_cal_date_register` datetime DEFAULT NULL,
  `mod_adv_cal_state` int NOT NULL DEFAULT '0' COMMENT '0. Eliminado, 1. Activo, 2. cancelado, 3. reagendado, 4. Atendido'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `mod_advised_calendars_blocked`
--

CREATE TABLE `mod_advised_calendars_blocked` (
  `mod_adv_cb_id` int NOT NULL,
  `mod_adv_cb_date_start` datetime NOT NULL,
  `mod_adv_cb_date_end` datetime DEFAULT NULL,
  `mod_adv_cb_ent_id` int DEFAULT NULL,
  `mod_adv_cb_state` int NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `mod_advised_counselors`
--

CREATE TABLE `mod_advised_counselors` (
  `mod_adv_cou_acu_id` int NOT NULL,
  `mod_adv_cou_cou_id` int NOT NULL,
  `mod_adv_cou_order` int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `mod_advised_counselors_fields`
--

CREATE TABLE `mod_advised_counselors_fields` (
  `mod_adv_fds_user_id` int NOT NULL,
  `mod_adv_fds_fds_id` int NOT NULL,
  `mod_adv_fds_order` int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `mod_advised_history`
--

CREATE TABLE `mod_advised_history` (
  `mod_adv_his_id` int NOT NULL,
  `mod_adv_his_acu_id` int DEFAULT NULL,
  `mod_adv_his_user_id` int DEFAULT NULL,
  `mod_adv_his_fds_id` int DEFAULT NULL,
  `mod_adv_his_comments` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci,
  `mod_adv_his_date_registration` datetime DEFAULT NULL,
  `mod_adv_his_date_init` datetime DEFAULT NULL,
  `mod_adv_his_date_end` datetime DEFAULT NULL,
  `mod_adv_his_place` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `mod_adv_his_type` int NOT NULL DEFAULT '0' COMMENT '0. normal 1.Fuera de Hora',
  `mod_adv_his_state` int NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `mod_advised_history_calendars`
--

CREATE TABLE `mod_advised_history_calendars` (
  `mod_adv_his_cal_cal_id` int NOT NULL,
  `mod_adv_his_cal_his_id` int NOT NULL,
  `mod_adv_his_cal_order` int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `mod_advised_options`
--

CREATE TABLE `mod_advised_options` (
  `mod_adv_opt_id` int NOT NULL,
  `mod_adv_opt_name` varchar(250) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `mod_adv_opt_value` varchar(250) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `mod_adv_opt_autoload` varchar(4) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT 'yes'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `mod_advised_schedules`
--

CREATE TABLE `mod_advised_schedules` (
  `mod_adv_sch_id` int NOT NULL,
  `mod_adv_sch_cou_id` int NOT NULL,
  `mod_adv_sch_day` int NOT NULL,
  `mod_adv_sch_hour_start` time NOT NULL,
  `mod_adv_sch_hour_end` time NOT NULL,
  `mod_adv_sch_count` int NOT NULL,
  `mod_adv_sch_state` int NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `mod_advised_types_appointments`
--

CREATE TABLE `mod_advised_types_appointments` (
  `mod_adv_tya_id` int NOT NULL,
  `mod_adv_tya_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `mod_adv_tya_summary` varchar(450) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `mod_adv_tya_type` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `mod_adv_tya_path` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `mod_adv_tya_color` varchar(8) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `mod_adv_tya_json` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci,
  `mod_adv_tya_ent_id` int DEFAULT NULL,
  `mod_adv_tya_state` int DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Volcado de datos para la tabla `mod_advised_types_appointments`
--

INSERT INTO `mod_advised_types_appointments` (`mod_adv_tya_id`, `mod_adv_tya_name`, `mod_adv_tya_summary`, `mod_adv_tya_type`, `mod_adv_tya_path`, `mod_adv_tya_color`, `mod_adv_tya_json`, `mod_adv_tya_ent_id`, `mod_adv_tya_state`) VALUES
(1, 'Consejeria', NULL, NULL, NULL, NULL, '{\r\n \"listUsers\",\"selectDay\"\r\n}', 1, 1),
(2, 'Otra Actividad', NULL, NULL, NULL, NULL, '{\r\n \"title\",\"selectDates\"\r\n}', 1, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `mod_advised_user`
--

CREATE TABLE `mod_advised_user` (
  `mod_adv_id` int NOT NULL,
  `mod_adv_fullname` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `mod_adv_comments` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `mod_adv_acu_id` int DEFAULT NULL,
  `mod_adv_fds_id` int DEFAULT NULL,
  `mod_adv_civil_state` int DEFAULT NULL COMMENT '1.Soltero/a 2.Casado/a 3.Divorciado 4.Concuvinato 5. Unión Libre  6. Otro',
  `mod_adv_dependents` int DEFAULT NULL,
  `mod_adv_telf_fax` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `mod_adv_address` varchar(550) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `mod_adv_coordinates` varchar(400) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `mod_adv_labor_status` int DEFAULT NULL COMMENT '1. Dependiente 2.Independiente. 3. Jubilado/rentista. 4.Estudiante 5. Desempleado',
  `mod_adv_company_work` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `mod_adv_address_work` varchar(550) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `mod_adv_work_area` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `mod_adv_profession` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `mod_adv_position` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `mod_adv_spouse_fullname` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `mod_adv_spouse_gender` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `mod_adv_spouse_ci` int DEFAULT NULL,
  `mod_adv_spouse_ext` varchar(2) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `mod_adv_spouse_birthday` date DEFAULT NULL,
  `mod_adv_spouse_nationality` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `mod_adv_spouse_telf_fax` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `mod_adv_spouse_cellphone` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `mod_adv_spouse_email` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `mod_adv_spouse_labor_status` int DEFAULT NULL,
  `mod_adv_spouse_company_work` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `mod_adv_spouse_address_work` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `mod_adv_spouse_work_area` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `mod_adv_spouse_profession` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `mod_adv_spouse_position` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `mod_brands`
--

CREATE TABLE `mod_brands` (
  `mod_brd_id` int NOT NULL,
  `mod_brd_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `mod_brd_img` int DEFAULT NULL,
  `mod_brd_state` int NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `mod_campaigns_actions`
--

CREATE TABLE `mod_campaigns_actions` (
  `mod_cpa_act_cpa_id` int NOT NULL,
  `mod_cpa_act_act_id` int NOT NULL,
  `mod_cpa_act_rew` int DEFAULT NULL,
  `mod_cpa_act_state` int NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `mod_campaigns_ads`
--

CREATE TABLE `mod_campaigns_ads` (
  `mod_cpa_id` int NOT NULL,
  `mod_cpa_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `mod_cpa_description` mediumtext CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci,
  `mod_cpa_aca_id` int DEFAULT NULL COMMENT '//mod_accounts_ads',
  `mod_cpa_target_init_age` int DEFAULT NULL,
  `mod_cpa_target_end_age` int DEFAULT NULL,
  `mod_cpa_target_gender` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `mod_cpa_init_date` datetime DEFAULT NULL,
  `mod_cpa_end_date` datetime DEFAULT NULL,
  `mod_cpa_acu_id` int DEFAULT NULL,
  `mod_cpa_register_date` datetime DEFAULT NULL,
  `mod_cpa_state` int NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `mod_carts`
--

CREATE TABLE `mod_carts` (
  `mod_cart_id` int NOT NULL,
  `mod_cart_date` datetime DEFAULT NULL,
  `mod_cart_code` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `mod_cart_acu_id` int NOT NULL,
  `mod_cart_add_id` int DEFAULT NULL,
  `mod_cart_ldz_id` int DEFAULT NULL,
  `mod_cart_details` varchar(455) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `mod_cart_payment_method` varchar(11) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `mod_cart_invoice_id` int DEFAULT NULL,
  `mod_cart_code_receipts` int DEFAULT NULL COMMENT 'Codigo de Recaudación\r\n',
  `mod_cart_transaction_id` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `mod_cart_gateway_url_payment` varchar(450) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `mod_cart_gateway_payment` varchar(40) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `mod_cart_gateway_url_invoice` varchar(450) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `mod_cart_gateway_json` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci,
  `mod_cart_state` int NOT NULL DEFAULT '0' COMMENT '0 Creada 1 Registrada 2. Pagada 3. Revertida 4. Anulada 4.Eliminada\r\n'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `mod_carts_products`
--

CREATE TABLE `mod_carts_products` (
  `mod_cart_prod_cart_id` int NOT NULL,
  `mod_cart_prod_prod_id` int NOT NULL,
  `mod_cart_prod_price` decimal(20,2) DEFAULT NULL,
  `mod_cart_prod_order` int DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `mod_classification_info`
--

CREATE TABLE `mod_classification_info` (
  `mod_clsi_id` int NOT NULL,
  `mod_clsi_name` varchar(250) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `mod_clsi_description` varchar(450) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `mod_clsi_color` varchar(11) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `mod_clsi_file_id` int DEFAULT NULL,
  `mod_clsi_state` int NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `mod_competitions`
--

CREATE TABLE `mod_competitions` (
  `mod_cmp_id` int NOT NULL,
  `mod_cmp_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `mod_cmp_description` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci,
  `mod_cmp_date_init` datetime DEFAULT CURRENT_TIMESTAMP,
  `mod_cmp_date_end` datetime DEFAULT CURRENT_TIMESTAMP,
  `mod_cmp_status` int NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `mod_competitions_accounts`
--

CREATE TABLE `mod_competitions_accounts` (
  `mod_cmp_cmp_id` int NOT NULL,
  `mod_cmp_acu_id` int NOT NULL,
  `mod_cmp_rel` int DEFAULT NULL,
  `mod_cmp_date_register` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `mod_competitions_mira`
--

CREATE TABLE `mod_competitions_mira` (
  `mod_cmp_mira_id` int NOT NULL,
  `mod_cmp_mira_user_id` int NOT NULL,
  `mod_cmp_mira_websocial` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `mod_cmp_mira_type` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT 'individual,grupo/colectiva',
  `mod_cmp_mira_incluye` int DEFAULT NULL COMMENT '0. Sí, se incluye (referencia, link, site, o adjunto)\r\n1. No, se incluye.\r\n2. Soy artista emergente.\r\n',
  `mod_cmp_mira_titulo` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `mod_cmp_mira_tipo_proyecto` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '(Fotografía, video, performance, instalación, objeto, escultura, pintura, etc.)',
  `mod_cmp_mira_sintesis` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci,
  `mod_cmp_mira_espacio` int NOT NULL COMMENT '(Tipología, características y dimensiones. Max. 500 caracteres)',
  `mod_cmp_mira_file_id` int DEFAULT NULL,
  `mod_cmp_mira_link` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `mod_cmp_mira_link_declaracion` int NOT NULL,
  `mod_cmp_mira_state` int DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `mod_customers_data`
--

CREATE TABLE `mod_customers_data` (
  `mod_ced_id` int NOT NULL,
  `mod_cep_business_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `mod_cep_nit` varchar(22) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `mod_cep_priority` int DEFAULT NULL,
  `mod_cep_type_contract` int DEFAULT NULL,
  `mod_cep_json` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Volcado de datos para la tabla `mod_customers_data`
--

INSERT INTO `mod_customers_data` (`mod_ced_id`, `mod_cep_business_name`, `mod_cep_nit`, `mod_cep_priority`, `mod_cep_type_contract`, `mod_cep_json`) VALUES
(1, 'Wappcom Srl.', '251636025', 2, 1, ''),
(2, 'Eurochronos SRL', '150628027', 1, 2, ''),
(3, 'BBVA Previsión AFP S.A', '1028339025', 1, 2, ''),
(4, 'Fundación UPSA', '1015227020', 1, 2, ''),
(5, 'Centro Cristiano Evangelico Casa de Oración', '1030635020', 1, 1, ''),
(6, 'Cabruja Films SRL', '285002020', 2, 1, ''),
(7, 'Modas Adela SRL', '401728021', 2, 1, ''),
(8, '', '', 2, 1, ''),
(9, 'Candire LS', '', 1, 2, ''),
(10, 'EDITORIAL DIA A DIA S.A.', '150782020', 2, 2, ''),
(11, 'Panadería Victoria Ltda', '1014779027', 2, 1, '');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `mod_customers_enterprises`
--

CREATE TABLE `mod_customers_enterprises` (
  `mod_cen_id` int NOT NULL,
  `mod_cen_cpe_id` int DEFAULT NULL,
  `mod_cen_ced_id` int DEFAULT NULL COMMENT 'customer data',
  `mod_cen_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `mod_cen_username` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `mod_cen_code` varchar(12) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `mod_cen_description` varchar(455) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `mod_cen_img` int DEFAULT NULL,
  `mod_cen_register_date` datetime DEFAULT NULL,
  `mod_cen_user_id` int DEFAULT NULL,
  `mod_cen_ent_id` int DEFAULT NULL,
  `mod_cen_state` int NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Volcado de datos para la tabla `mod_customers_enterprises`
--

INSERT INTO `mod_customers_enterprises` (`mod_cen_id`, `mod_cen_cpe_id`, `mod_cen_ced_id`, `mod_cen_name`, `mod_cen_username`, `mod_cen_code`, `mod_cen_description`, `mod_cen_img`, `mod_cen_register_date`, `mod_cen_user_id`, `mod_cen_ent_id`, `mod_cen_state`) VALUES
(1, 0, 1, 'Wappcom', 'wappcom', 'wapp', NULL, 0, '2023-01-03 15:50:12', 1, 1, 1),
(2, 0, 2, 'Eurochronos', 'eurochronos', 'euro', 'Paola Andrea Abudinen	77801315	marketing@eurochronos.com.bo', 0, '2023-01-03 18:08:14', 1, 1, 1),
(3, 0, 3, 'Previsión', 'prevision', 'prevision', 'Magali Justiniano	70844811	mjustiniano@prevision.com.bo', 0, '2023-01-03 19:55:00', 1, 1, 1),
(4, 0, 4, 'Cenace', 'cenace', 'cenace', 'Gabriela Roca Aguilera	76600069	gabrielaroca@upsa.edu.bo', 0, '2023-01-03 19:56:14', 1, 1, 1),
(5, 0, 5, 'Casa de Oración', 'casadeoracion', 'casaoracion', 'Gabriel Rosales Jándula	75001599	gabriel.rosales.j@gmail.com', 0, '2023-01-03 20:07:04', 1, 1, 1),
(6, 0, 6, 'Cabruja Films', 'cabruja', 'cabruja', 'Jose Luis Cabruja	72121870	jlcabruja@gmail.com	Daniela Gutierrez		dgcabrujafilms@gmail.com', 0, '2023-01-03 20:10:03', 1, 1, 1),
(7, 0, 7, 'Francisco Arce', 'franciscoarce', 'FA001', 'Francisco Arce 76342483 farcime@gmail.com', 0, '2023-01-03 20:24:58', 1, 1, 1),
(8, 0, 8, 'Chaplin Show', 'chaplinshow', 'CHS-001', '', 0, '2023-01-03 20:57:24', 1, 1, 1),
(9, 0, 9, 'Candire', 'candire', 'CAN-001', 'Bernardo Daza +591 67702930 bdaza@candire.net', 0, '2023-06-01 14:53:39', 1, 1, 1),
(10, 0, 10, 'Edadsa', 'edadsa', 'EDA-001', 'Carlos Lopez clopez@eldia.com.bo  +591 65060735', 0, '2023-09-06 16:45:41', 1, 1, 1),
(11, 0, 11, 'Victoria', 'victoria', 'VIC-001', 'Luis Fernando Villa	3 348-7070 int 1610 jefesistemas@panaderiavictoria.com', 0, '2023-09-19 09:05:47', 1, 1, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `mod_customers_persons`
--

CREATE TABLE `mod_customers_persons` (
  `mod_cpe_id` int NOT NULL,
  `mod_cpe_acu_id` int DEFAULT NULL,
  `mod_cpe_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `mod_cpe_lastname` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `mod_cpe_email` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `mod_cpe_code` varchar(12) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `mod_cpe_phone` int DEFAULT NULL,
  `mod_cpe_ci` int DEFAULT NULL,
  `mod_cpe_adder` varchar(12) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `mod_cpe_register_date` datetime NOT NULL,
  `mod_cpe_user_id` int DEFAULT NULL COMMENT 'usuario que lo registro\r\n',
  `mod_cpe_ent_id` int DEFAULT NULL,
  `mod_cpe_state` int NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `mod_departments`
--

CREATE TABLE `mod_departments` (
  `mod_dep_id` int NOT NULL,
  `mod_dep_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `mod_dep_parent_id` int NOT NULL,
  `mod_dep_description` tinytext CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `mod_dep_ent_id` int NOT NULL,
  `mod_dep_order` int NOT NULL,
  `mod_dep_state` int NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `mod_discount`
--

CREATE TABLE `mod_discount` (
  `mod_dis_id` int NOT NULL,
  `mod_dis_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `mod_dis_detail` tinytext CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci,
  `mod_dis_rate` int DEFAULT NULL,
  `mod_dis_start_date` datetime DEFAULT NULL,
  `mod_dis_end_date` datetime DEFAULT NULL,
  `mod_dis_state` int NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `mod_discount_products`
--

CREATE TABLE `mod_discount_products` (
  `mod_dis_prod_dis_id` int NOT NULL,
  `mod_dis_prod_prod_id` int NOT NULL,
  `mod_dis_prod_orden` int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `mod_events`
--

CREATE TABLE `mod_events` (
  `mod_eve_id` int NOT NULL,
  `mod_eve_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `mod_eve_description` tinytext CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci,
  `mod_eve_register_date` datetime DEFAULT NULL,
  `mod_eve_user_id` int DEFAULT NULL,
  `mod_eve_init_date` datetime DEFAULT NULL,
  `mod_eve_end_date` datetime DEFAULT NULL,
  `mod_eve_img_small` int DEFAULT NULL,
  `mod_eve_img_medium` int DEFAULT NULL,
  `mod_eve_img_large` int DEFAULT NULL,
  `mod_eve_seats` int DEFAULT NULL,
  `mod_eve_hall` int DEFAULT NULL,
  `mod_eve_json` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci,
  `mod_eve_cost_seat` decimal(21,2) DEFAULT NULL,
  `mod_eve_coin` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT 'Bs',
  `mod_eve_ent_id` int DEFAULT NULL,
  `mod_eve_state` int NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `mod_events_halls`
--

CREATE TABLE `mod_events_halls` (
  `mod_eve_hall_id` int NOT NULL,
  `mod_eve_hall_ent_id` int NOT NULL,
  `mod_eve_hall_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `mod_eve_hall_details` tinytext CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci,
  `mod_eve_hall_url_draw_id` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `mod_eve_hall_fn_js_draw` varchar(440) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `mod_eve_hall_num_tables` int DEFAULT NULL,
  `mod_eve_hall_num_seats` int DEFAULT NULL,
  `mod_eve_hall_address` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `mod_eve_hall_city` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `mod_eve_hall_country` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `mod_eve_hall_coord` varchar(440) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `mod_eve_hall_user_id` int DEFAULT NULL,
  `mod_eve_hall_register_date` datetime DEFAULT NULL,
  `mod_eve_hall_state` int NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `mod_events_options`
--

CREATE TABLE `mod_events_options` (
  `mod_eve_option_id` int NOT NULL,
  `mod_eve_option_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `mod_eve_option_value` int DEFAULT NULL,
  `mod_eve_option_autoload` varchar(3) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `mod_events_tickets`
--

CREATE TABLE `mod_events_tickets` (
  `mod_eve_tck_id` int NOT NULL,
  `mod_eve_tck_eve_id` int DEFAULT NULL,
  `mod_eve_tck_hall_id` int DEFAULT NULL,
  `mod_eve_tck_table_id` int DEFAULT NULL COMMENT 'mesa id',
  `mod_eve_tck_seat_id` int DEFAULT NULL,
  `mod_eve_tck_code` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `mod_eve_tck_ref` varchar(40) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT 'reference',
  `mod_eve_tck_reserve_id` int NOT NULL,
  `mod_eve_tck_acp_id` int DEFAULT NULL COMMENT 'mod_accounting_plan id\r\n',
  `mod_eve_tck_cpe_id` int DEFAULT NULL,
  `mod_eve_tck_date_register` datetime DEFAULT NULL,
  `mod_eve_tck_cost` decimal(22,2) DEFAULT NULL,
  `mod_eve_tck_coin` varchar(3) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `mod_eve_tck_user_id` int NOT NULL,
  `mod_eve_tck_pay_tx_id` int DEFAULT NULL COMMENT 'mod_pay_tx_tck_id of mod_payments_tx_tickets',
  `mod_eve_tck_date_pay_register` datetime DEFAULT NULL,
  `mod_eve_tck_txr` int DEFAULT NULL COMMENT 'id_transaccion pasarela',
  `mod_eve_tck_state` int NOT NULL DEFAULT '0' COMMENT '// State 0:Libre, 1.temp 2:Recervado, 3: Vendido, 4:Anulado, 5:Cancelado  Null : sin estado '
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `mod_external_docs`
--

CREATE TABLE `mod_external_docs` (
  `mod_edc_id` int NOT NULL,
  `mod_edc_site` varchar(45) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `mod_edc_user_id` int DEFAULT NULL,
  `mod_edc_approver_id` int DEFAULT NULL,
  `mod_edc_tpl_id` int DEFAULT NULL,
  `mod_edc_date_register` datetime DEFAULT NULL,
  `mod_edc_key` varchar(400) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `mod_edc_clsi_id` int DEFAULT NULL COMMENT 'mod_classification_info',
  `mod_edc_code` varchar(45) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `mod_edc_pw` varchar(400) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `mod_edc_data` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci,
  `mod_edc_flags` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `mod_edc_json` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci,
  `mod_edc_state` int DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `mod_external_docs_options`
--

CREATE TABLE `mod_external_docs_options` (
  `mod_edc_opt_id` int NOT NULL,
  `mod_edc_opt_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `mod_edc_opt_value` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `mod_edc_opt_ent_id` int NOT NULL,
  `mod_edc_opt_autoload` varchar(4) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `mod_external_docs_page_design`
--

CREATE TABLE `mod_external_docs_page_design` (
  `mod_edc_pd_id` int NOT NULL,
  `mod_edc_pd_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `mod_edc_pd_url` varchar(400) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `mod_edc_pd_json` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci,
  `mod_edc_pd_ent_id` int DEFAULT NULL,
  `mod_edc_pd_state` int NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `mod_external_docs_templates`
--

CREATE TABLE `mod_external_docs_templates` (
  `mod_edc_tpl_id` int NOT NULL,
  `mod_edc_tpl_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `mod_edc_tpl_description` varchar(445) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `mod_edc_tpl_code` varchar(11) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `mod_edc_tpl_tags` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `mod_edc_tpl_type_id` int DEFAULT NULL COMMENT 'mod_external_docs_types',
  `mod_edc_tpl_pd_id` int DEFAULT NULL COMMENT 'mod_external_docs_page_design',
  `mod_edc_tpl_body` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci,
  `mod_edc_tpl_register_user_id` int DEFAULT NULL,
  `mod_edc_tpl_register_date` datetime DEFAULT NULL,
  `mod_edc_tpl_json` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci,
  `mod_edc_tpl_ent_id` int DEFAULT NULL,
  `mod_edc_tpl_state` int NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `mod_external_docs_types`
--

CREATE TABLE `mod_external_docs_types` (
  `mod_edc_type_id` int NOT NULL,
  `mod_edc_type_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `mod_edc_type_description` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `mod_edc_type_parent_id` int NOT NULL,
  `mod_edc_type_ent_id` int NOT NULL,
  `mod_edc_type_color` varchar(8) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `mod_edc_type_state` int NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `mod_inventory_options`
--

CREATE TABLE `mod_inventory_options` (
  `mod_inv_opt_id` int NOT NULL,
  `mod_inv_opt_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `mod_inv_opt_value` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci,
  `mod_inv_opt_autoload` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `mod_inv_opt_ent_id` int DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Volcado de datos para la tabla `mod_inventory_options`
--

INSERT INTO `mod_inventory_options` (`mod_inv_opt_id`, `mod_inv_opt_name`, `mod_inv_opt_value`, `mod_inv_opt_autoload`, `mod_inv_opt_ent_id`) VALUES
(1, 'routeCategoryId', '3', '', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `mod_inventory_units`
--

CREATE TABLE `mod_inventory_units` (
  `mod_inv_unt_id` int NOT NULL,
  `mod_inv_unt_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `mod_inv_unt_summary` varchar(455) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `mod_inv_unt_vars` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci,
  `mod_inv_unt_state` int NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Volcado de datos para la tabla `mod_inventory_units`
--

INSERT INTO `mod_inventory_units` (`mod_inv_unt_id`, `mod_inv_unt_name`, `mod_inv_unt_summary`, `mod_inv_unt_vars`, `mod_inv_unt_state`) VALUES
(1, 'QUINTAL', '', '', 1),
(2, 'AGUJA', '', '', 1),
(3, 'AMPOLLA', '', '', 1),
(4, 'BIDÓN', '', '', 1),
(5, 'BOLSA', '', '', 1),
(6, 'CAPSULA', '', '', 1),
(7, 'CARTUCHO', '', '', 1),
(8, 'COMPRIMIDO', '', '', 1),
(9, 'CENTIMETRO LINEAL', '', '', 1),
(10, 'CIENTO DE UNIDADES', '', '', 1),
(11, 'CILINDRO', '', '', 1),
(12, 'CONOS', '', '', 1),
(13, 'DOCENA', '', '', 1),
(14, 'GALON INGLES', '', '', 1),
(15, 'HECTOLITRO', '', '', 1),
(16, 'JUEGO', '', '', 1),
(17, 'UNIDAD (BIENES)', '', '', 1),
(18, 'DIAS', '', '', 1),
(19, 'BALDE', '', '', 1),
(20, 'BARRILES', '', '', 1),
(21, 'ONZAS', '', '', 1),
(22, 'PALETAS', '', '', 1),
(23, 'PAQUETE', '', '', 1),
(24, 'PAR', '', '', 1),
(25, 'PIES', '', '', 1),
(26, 'PIES CUADRADOS', '', '', 1),
(27, 'PIES CUBICOS', '', '', 1),
(28, 'PIEZAS', '', '', 1),
(29, 'PLACAS', '', '', 1),
(30, 'PLIEGO', '', '', 1),
(31, 'ONZA TROY', '', '', 1),
(32, 'LIBRA FINA', '', '', 1),
(33, 'DISPLAY', '', '', 1),
(34, 'BULTO', '', '', 1),
(35, 'MESES', '', '', 1),
(36, 'PULGADAS', '', '', 1),
(37, 'RESMA', '', '', 1),
(38, 'TAMBOR', '', '', 1),
(39, 'TONELADA CORTA', '', '', 1),
(40, 'TONELADA LARGA', '', '', 1),
(41, 'TONELADAS', '', '', 1),
(42, 'TUBOS', '', '', 1),
(43, 'UNIDAD (SERVICIOS)', '', '', 1),
(44, 'US GALON (3,7843 L)', '', '', 1),
(45, 'YARDA', '', '', 1),
(46, 'YARDA CUADRADA', '', '', 1),
(47, 'OTRO', '', '', 1),
(48, 'BOLSA', '', '', 1),
(49, 'BOTELLAS', '', '', 1),
(50, 'FARDO', '', '', 1),
(51, 'GRAMO', '', '', 1),
(52, 'GRUESA', '', '', 1),
(53, 'HOJA', '', '', 1),
(54, 'KILOGRAMO', '', '', 1),
(55, 'BOBINAS', '', '', 1),
(56, 'CAJA', '', '', 1),
(57, 'CARTONES', '', '', 1),
(58, 'CENTIMETRO CUADRADO', '', '', 1),
(59, 'CENTIMETRO CUBICO', '', '', 1),
(60, 'ESTUCHE', '', '', 1),
(61, 'KILOVATIO HORA', '', '', 1),
(62, 'KIT', '', '', 1),
(63, 'LATAS', '', '', 1),
(64, 'LIBRAS', '', '', 1),
(65, 'LITRO', '', '', 1),
(66, 'MEGAWATT HORA', '', '', 1),
(67, 'METRO', '', '', 1),
(68, 'METRO CUADRADO', '', '', 1),
(69, 'METRO CUBICO', '', '', 1),
(70, 'MILIGRAMOS', '', '', 1),
(71, 'MILILITRO', '', '', 1),
(72, 'MILIMETRO', '', '', 1),
(73, 'MILIMETRO CUADRADO', '', '', 1),
(74, 'MILIMETRO CUBICO', '', '', 1),
(75, 'MILLARES', '', '', 1),
(76, 'MILLON DE UNIDADES', '', '', 1),
(77, 'KILOMETRO', '', '', 1),
(78, 'MEGAWATT', '', '', 1),
(79, 'KILOWATT', '', '', 1),
(80, 'FRASCO', '', '', 1),
(81, 'JERINGA', '', '', 1),
(82, 'MINI BOTELLA', '', '', 1),
(83, 'SACHET', '', '', 1),
(84, 'TABLETA', '', '', 1),
(85, 'TERMO', '', '', 1),
(86, 'TUBO', '', '', 1),
(87, 'ROLLO', '', '', 1),
(88, 'HORAS', '', '', 1),
(89, 'AMORTIZACION', '', '', 1),
(90, 'OVULOS', '', '', 1),
(91, 'SUPOSITORIOS', '', '', 1),
(92, 'SOBRES', '', '', 1),
(93, 'VIAL', '', '', 1),
(94, 'ARROBA', '', '', 1),
(95, 'HECTAREAS', '', '', 1),
(96, 'TONELADA METRICA', '', '', 1),
(97, 'EQUIPOS', '', '', 1),
(98, 'TIRA', '', '', 1),
(99, 'BLISTER', '', '', 1),
(100, 'TURRIL', '', '', 1),
(101, 'BANDEJA', '', '', 1),
(102, 'YARDA', '', '', 1),
(103, 'JABA', '', '', 1),
(104, 'CARTOLA', '', '', 1),
(105, 'TETRAPACK', '', '', 1),
(106, 'VASO', '', '', 1),
(107, 'POMO', '', '', 1),
(108, 'UNIDAD TERMICA BRITANICA (TI)', '', '', 1),
(109, 'MILLONES DE BTU (1000000 BTU)', '', '', 1),
(110, 'MILLONES DE PIES CUBICOS (1000000 PC)', '', '', 1),
(111, 'MILLAR DE PIES CUBICOS (1000 PC)', '', '', 1),
(112, 'MIL PIES CUBICOS 14696 PSI 68FAH', '', '', 1),
(113, 'MIL PIES CUBICOS 14696 PSI', '', '', 1),
(114, 'METRO CUBICO 68F VOL', '', '', 1),
(115, 'BARRIL [42 GALONES(EEUU)]', '', '', 1),
(116, 'BARRIL (EEUU) 60 F', '', '', 1),
(117, 'AEROSOL', '', '', 1),
(118, 'BARRA', '', '', 1),
(119, 'CONJUNTO', '', '', 1),
(120, 'FANEGA', '', '', 1),
(121, 'PACK', '', '', 1),
(122, 'PASTILLA', '', '', 1),
(123, 'PIPETA', '', '', 1),
(124, 'POTE', '', '', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `mod_jobtitle`
--

CREATE TABLE `mod_jobtitle` (
  `mod_jbt_id` int NOT NULL,
  `mod_jbt_title` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `mod_jbt_description` tinytext CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `mod_jbt_parent_id` int NOT NULL,
  `mod_jpt_duties_manual` int NOT NULL,
  `mod_jbt_ent_id` int NOT NULL,
  `mod_jbt_order` int NOT NULL,
  `mod_jbt_state` int NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `mod_kardex`
--

CREATE TABLE `mod_kardex` (
  `mod_kdx_id` int NOT NULL,
  `mod_kdx_user_id` int NOT NULL,
  `mod_kdx_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `mod_kdx_fathers_lastname` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `mod_kdx_mothers_lastname` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `mod_kdx_ent_id` int DEFAULT NULL,
  `mod_kdx_entry_date` date DEFAULT NULL,
  `mod_kdx_ci` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `mod_kdx_ci_extension` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `mod_kdx_ci_expiration_date` date DEFAULT NULL,
  `mod_kdx_drivers_licence_expiration_date` date DEFAULT NULL,
  `mod_kdx_birth_date` date DEFAULT NULL,
  `mod_kdx_nationality` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `mod_kdx_birth_place` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `mod_kdx_sex` int DEFAULT NULL COMMENT '2 Masculino, 1 Femenino, 3. Otros',
  `mod_kdx_civil_status` int DEFAULT NULL COMMENT '0.single 1.married 2.divorced 3.widowed',
  `mod_kdx_dates_contact` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci,
  `mod_kdx_phone_address` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `mod_kdx_phone_corp` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `mod_kdx_phone_corp_ext` int DEFAULT NULL,
  `mod_kdx_personal_mobile` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `mod_kdx_corp_mobile` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `mod_kdx_personal_email` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `mod_kdx_corp_email` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `mod_kdx_address` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `mod_kdx_coordinates` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `mod_kdx_nro_cns` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `mod_kdx_nro_afp` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `mod_kdx_afp` int DEFAULT NULL,
  `mod_kdx_shirt_size` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `mod_kdx_pant_size` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `mod_kdx_boot_size` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `mod_kdx_blood_type` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `mod_kdx_removal_date` date DEFAULT NULL,
  `mod_kdx_cod_sap` int DEFAULT NULL,
  `mod_kdx_cv_id` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `mod_kdx_workhours` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `mod_kdx_trash` int DEFAULT NULL,
  `mod_kdx_state` int DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `mod_kardex_jobtitle`
--

CREATE TABLE `mod_kardex_jobtitle` (
  `mod_kdx_jbt_jbt_id` int NOT NULL,
  `mod_kdx_jbt_kdx_id` int NOT NULL,
  `mod_kdx_jbt_ent_id` int NOT NULL,
  `mod_kdx_jbt_order` int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `mod_leagues`
--

CREATE TABLE `mod_leagues` (
  `mod_lg_id` int NOT NULL,
  `mod_lg_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `mod_lg_description` varchar(455) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `mod_lg_sport` varchar(120) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `mod_lg_json` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci,
  `mod_lg_ent_id` int DEFAULT NULL,
  `mod_lg_state` int NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `mod_leagues_divisions`
--

CREATE TABLE `mod_leagues_divisions` (
  `mod_lg_div_id` int NOT NULL,
  `mod_lg_div_lg_id` int DEFAULT NULL COMMENT 'Id league',
  `mod_lg_div_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `mod_lg_div_description` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci,
  `mod_lg_div_json` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci,
  `mod_lg_div_state` int DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `mod_leagues_divisions_teams`
--

CREATE TABLE `mod_leagues_divisions_teams` (
  `mod_lg_div_tm_div_id` int NOT NULL,
  `mod_lg_div_tm_tm_id` int NOT NULL,
  `mod_lg_div_tm_order` int DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `mod_logistics_distribution_move`
--

CREATE TABLE `mod_logistics_distribution_move` (
  `mod_log_dtz_id` int NOT NULL,
  `mod_log_dtz_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `mod_log_dtz_code` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `mod_log_dtz_perimetrer` mediumtext CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci,
  `mod_log_dtz_cost` decimal(20,2) NOT NULL,
  `mod_log_dtz_state` int DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `mod_logistics_distribution_zone`
--

CREATE TABLE `mod_logistics_distribution_zone` (
  `mod_log_dtz_id` int NOT NULL,
  `mod_log_dtz_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `mod_log_dtz_code` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `mod_log_dtz_perimetrer` mediumtext CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci,
  `mod_log_dtz_cost` decimal(20,2) NOT NULL,
  `mod_log_dtz_state` int DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `mod_make`
--

CREATE TABLE `mod_make` (
  `mod_mak_id` int NOT NULL,
  `mod_mak_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `mod_mak_description` varchar(400) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `mod_mak_img_id` int NOT NULL,
  `mod_mak_state` int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `mod_organization_chart`
--

CREATE TABLE `mod_organization_chart` (
  `mod_orc_id` int NOT NULL,
  `mod_orc_position` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `mod_orc_json` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci,
  `mod_orc_functions` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci,
  `mod_orc_doc_id` int DEFAULT NULL COMMENT 'rel functions',
  `mod_orc_location` int NOT NULL,
  `mod_orc_reports_to` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT 'id,id,id',
  `mod_orc_code` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `mod_orc_user_id` int NOT NULL COMMENT 'id register',
  `mod_orc_date_register` datetime DEFAULT NULL,
  `mod_orc_ent_id` int DEFAULT NULL,
  `mod_orc_state` int NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `mod_organization_chart_rel`
--

CREATE TABLE `mod_organization_chart_rel` (
  `mod_orc_rel_orc_id` int NOT NULL,
  `mod_orc_rel_user_id` int NOT NULL,
  `mod_orc_rel_kdx_id` int DEFAULT NULL,
  `mod_orc_rel_date_register` date DEFAULT NULL,
  `mod_orc_rel_order` int DEFAULT NULL,
  `mod_org_rel_state` int NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `mod_payments_tx_tickets`
--

CREATE TABLE `mod_payments_tx_tickets` (
  `mod_pay_tx_tck_id` int NOT NULL,
  `mod_pay_tx_tck_acp_id` int DEFAULT NULL,
  `mod_pay_tx_tck_cpe_id` int NOT NULL,
  `mod_pay_tx_tck_eve_id` int NOT NULL,
  `mod_pay_tx_tck_reserve_id` int NOT NULL,
  `mod_pay_tx_tck_pay` float(22,2) NOT NULL DEFAULT '0.00',
  `mod_pay_tx_tck_code` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `mod_pay_tx_tck_coin` varchar(4) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `mod_pay_tx_tck_rs` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT 'razon social',
  `mod_pay_tx_tck_nit` int DEFAULT NULL,
  `mod_pay_tx_tck_type` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `mod_pay_tx_tck_txr` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT 'id transaccion',
  `mod_pay_tx_tck_date_pay_register` datetime NOT NULL,
  `mod_pay_tx_tck_callback` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci,
  `mod_pay_tx_tck_mail` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci,
  `mod_pay_tx_tck_user_id` int NOT NULL DEFAULT '0',
  `mod_pay_tx_tck_json` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci,
  `mod_pay_tx_tck_return` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci,
  `mod_pay_tx_tck_register_date` datetime DEFAULT NULL,
  `mod_pay_tx_tck_state` int NOT NULL DEFAULT '0' COMMENT '0. Iniciado, 2.registrado, 3.Active/pagado, 4.Anulado 5.Delete'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `mod_places`
--

CREATE TABLE `mod_places` (
  `mod_plc_id` int NOT NULL,
  `mod_plc_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `mod_plc_tags` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `mod_plc_json` mediumtext CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci,
  `mod_plc_address` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT 'ej: Av. 3 pasos al frente, Calle Isoso #34 Edificio Lourdes Dep. 54',
  `mod_plc_city` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `mod_plc_country` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `mod_plc_phone` varchar(15) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `mod_plc_celular_whatsapp` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `mod_plc_info` mediumtext CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `mod_plc_img` int NOT NULL,
  `mod_plc_profile_icon` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `mod_plc_main_coord` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `mod_plc_perimeter` varchar(455) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `mod_plc_map_icon` varchar(55) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `mod_plc_content` mediumtext CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `mod_plc_acu_id` int NOT NULL DEFAULT '0',
  `mod_plc_cont_id` int NOT NULL DEFAULT '0' COMMENT 'contenido_id',
  `mod_plc_status` int NOT NULL COMMENT 'the situation at a particular time during a process.',
  `mod_plc_ranking` int NOT NULL DEFAULT '5',
  `mod_plc_code` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `mod_plc_map_ubication` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `mod_plc_24h` int NOT NULL,
  `mod_plc_ent_id` int DEFAULT NULL,
  `mod_plc_user_id` int DEFAULT NULL,
  `mod_plc_state` int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `mod_places_accounts_ads`
--

CREATE TABLE `mod_places_accounts_ads` (
  `mod_plc_aca_aca_id` int NOT NULL,
  `mod_plc_aca_plc_id` int NOT NULL,
  `mod_plc_aca_date_register` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `mod_places_categorys`
--

CREATE TABLE `mod_places_categorys` (
  `mod_plc_cat_plc_id` int NOT NULL,
  `mod_plc_cat_cat_id` int NOT NULL,
  `mod_plc_cat_order` int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `mod_prices_list`
--

CREATE TABLE `mod_prices_list` (
  `mod_prl_id` int NOT NULL,
  `mod_prl_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `mod_prl_description` varchar(450) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `mod_prl_user_id` int DEFAULT NULL,
  `mod_prl_base_list` int DEFAULT NULL COMMENT 'lista base id\r\n\r\n',
  `mod_prl_factor` float(11,3) DEFAULT '1.000',
  `mod_prl_rounding_method` int NOT NULL COMMENT '0. sin *  1: si el siguiente dígito hacia la derecha después del último que desea conservarse es menor a 5, entonces el último no debe ser modificado. Por ejemplo: 8,453 se convertiría en 8,45;\r\n\r\n*2: en el caso opuesto al anterior, cuando el dígito siguiente al límite es mayor a 5, el último se debe incrementar en una unidad. Por ejemplo: 8,459 se convertiría en 8,46;\r\n\r\n*3: si un 5 sigue al último dígito que desea conservarse y después del 5 hay al menos un número diferente de 0, el último se debe incrementar en una unidad. Por ejemplo: 6,345070 se convertiría en 6,35;\r\n\r\n*4 si el último dígito deseado es un número par y a su derecha hay un 5 como dígito final o seguido de ceros, entonces no se realizan más cambios que el mero truncamiento. Por ejemplo, 4,32500 y 4,325 pasarían a ser 4,32;\r\n\r\n*\r\n5: de manera opuesta a la regla anterior, si el último dígito requerido es un número impar, entonces debemos aumentarlo en una unidad. Por ejemplo: 4,31500 y 4,315 se convertirían en 4,32. \r\n',
  `mod_prl_register_date` datetime NOT NULL,
  `mod_prl_ent_id` int DEFAULT NULL,
  `mod_prl_state` int NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `mod_producers`
--

CREATE TABLE `mod_producers` (
  `mod_prd_id` int NOT NULL,
  `mod_prd_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `mod_prd_description` varchar(400) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `mod_prd_img_id` int NOT NULL,
  `mod_prd_state` int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `mod_products`
--

CREATE TABLE `mod_products` (
  `mod_prod_id` int NOT NULL,
  `mod_prod_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `mod_prod_pathurl` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `mod_prod_description` varchar(445) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `mod_prod_tags` tinytext CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci,
  `mod_prod_img` int DEFAULT NULL,
  `mod_prod_code` varchar(45) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `mod_prod_record_date` datetime DEFAULT NULL,
  `mod_prod_type` int NOT NULL DEFAULT '0',
  `mod_prod_state` int NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `mod_products_categorys`
--

CREATE TABLE `mod_products_categorys` (
  `mod_prod_cat_prod_id` int NOT NULL,
  `mod_prod_cat_cat_id` int NOT NULL,
  `mod_prod_cat_ent_id` int NOT NULL,
  `mod_prod_cat_order` int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `mod_products_clothing_store`
--

CREATE TABLE `mod_products_clothing_store` (
  `mod_prod_cst_id` int NOT NULL,
  `mod_prod_cst_prod_id` int NOT NULL,
  `mod_prod_cst_size` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT 'sx,l,m,l',
  `mod_prod_cst_codebar` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `mod_prod_cst_contents` int NOT NULL,
  `mod_prod_cst_colors` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci COMMENT 'json',
  `mod_prop_cst_notes` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `mod_products_colors`
--

CREATE TABLE `mod_products_colors` (
  `mod_prod_color_id` int NOT NULL,
  `mod_prod_color_name` varchar(25) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `mod_prod_color_hex` varchar(11) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `mod_prod_color_json` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci,
  `mod_prod_color_ent_id` int NOT NULL,
  `mod_prod_color_state` int NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `mod_products_files`
--

CREATE TABLE `mod_products_files` (
  `mod_prod_file_prod_id` int NOT NULL,
  `mod_prod_file_file_id` int NOT NULL,
  `mod_prod_file_order` int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `mod_products_food`
--

CREATE TABLE `mod_products_food` (
  `mod_prod_food_id` int NOT NULL,
  `mod_prod_food_prod_id` int DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `mod_products_options`
--

CREATE TABLE `mod_products_options` (
  `mod_prod_opt_id` int NOT NULL,
  `mod_prod_opt_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `mod_prod_opt_value` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `mod_prod_opt_autoload` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `mod_products_pharmacy`
--

CREATE TABLE `mod_products_pharmacy` (
  `mod_prod_pha_id` int NOT NULL,
  `mod_prod_pha_code_provider` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `mod_prod_pha_prov_id` int NOT NULL,
  `mod_prod_pha_lab_id` int NOT NULL,
  `mod_prod_pha_mak_id` int NOT NULL,
  `mod_prod_pha_presentation` varchar(400) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `mod_prod_pha_codebar` varchar(400) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `mod_prod_pha_phar_form` varchar(400) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `mod_prod_pha_active_concentration_1` varchar(400) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `mod_prod_pha_active_concentration_2` varchar(400) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `mod_prod_pha_active_concentration_3` varchar(400) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `mod_prod_pha_therapy_action` varchar(400) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `mod_prod_pha_condition_sale` int NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `mod_products_pharmacy_stock`
--

CREATE TABLE `mod_products_pharmacy_stock` (
  `mod_prod_pha_stk_id` int NOT NULL,
  `mod_prod_pha_stk_contry_id` varchar(400) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `mod_prod_pha_stk_nro_health_registration` varchar(400) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `mod_prod_pha_stk_expiration_date` datetime NOT NULL,
  `mod_prod_pha_stk_registration_date` datetime NOT NULL,
  `mod_prod_pha_stk_purchase_unit` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `mod_prod_pha_stk_ratio_units_box` int NOT NULL,
  `mod_prod_pha_stk_lot` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `mod_prod_pha_stk_box_blister_ratio` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `mod_prod_pha_stk_height_cm` int NOT NULL,
  `mod_prod_pha_stk_width_cm` int NOT NULL,
  `mod_prod_pha_stk_depth_cm` int NOT NULL,
  `mod_prod_pha_stk_diameter_cm` int NOT NULL,
  `mod_prod_pha_stk_storage_temperature_c` varchar(400) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `mod_products_prices_list`
--

CREATE TABLE `mod_products_prices_list` (
  `mod_prod_prl_id` int NOT NULL,
  `mod_prod_prl_prod_id` int NOT NULL,
  `mod_prod_prl_price` decimal(20,2) NOT NULL,
  `mod_prod_prl_previous_price` decimal(20,2) DEFAULT NULL,
  `mod_prod_prl_coin` varchar(5) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `mod_prod_prl_register_date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `mod_products_relation`
--

CREATE TABLE `mod_products_relation` (
  `mod_prod_rel_id` int NOT NULL,
  `mod_prod_rel_prod_id` int NOT NULL,
  `mod_prod_rel_ent_id` int NOT NULL,
  `mod_prod_rel_type` int DEFAULT NULL,
  `mod_prod_rel_stock` int DEFAULT NULL,
  `mod_prod_rel_pricing` int DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `mod_products_types`
--

CREATE TABLE `mod_products_types` (
  `mod_prod_type_id` int NOT NULL,
  `mod_prod_type_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `mod_prod_type_path` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `mod_prod_type_subfix` varchar(5) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `mod_prod_type_subfix_db` varchar(24) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `mod_prod_type_primary` int DEFAULT '0',
  `mod_prod_type_json` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci,
  `mod_prod_type_ent_id` int DEFAULT NULL,
  `mod_prod_type_state` int NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Volcado de datos para la tabla `mod_products_types`
--

INSERT INTO `mod_products_types` (`mod_prod_type_id`, `mod_prod_type_name`, `mod_prod_type_path`, `mod_prod_type_subfix`, `mod_prod_type_subfix_db`, `mod_prod_type_primary`, `mod_prod_type_json`, `mod_prod_type_ent_id`, `mod_prod_type_state`) VALUES
(1, 'Store', 'store', '_str', '_store', 1, '', 1, 1),
(2, 'Clothing store', 'clothingStore', '_cst', '_clothing_store', 0, NULL, 1, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `mod_products_warehouses`
--

CREATE TABLE `mod_products_warehouses` (
  `mod_prod_wrh_prod_id` int NOT NULL,
  `mod_prod_wrh_wrh_id` int NOT NULL,
  `mod_prod_wrh_order` int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `mod_providers`
--

CREATE TABLE `mod_providers` (
  `mod_prv_id` int NOT NULL,
  `mod_prv_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `mod_prv_description` varchar(450) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `mod_prv_img_id` int NOT NULL,
  `mod_prv_type` int NOT NULL,
  `mod_prv_register_date` datetime DEFAULT NULL,
  `mod_prv_user_id` int NOT NULL COMMENT 'Usuario que registra',
  `mod_prv_state` int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `mod_rehabilitation_centers`
--

CREATE TABLE `mod_rehabilitation_centers` (
  `mod_rhb_center_id` int NOT NULL,
  `mod_rhb_center_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `mod_rhb_center_address` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `mod_rhb_center_coord` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `mod_rhb_center_user_id` int DEFAULT NULL,
  `mod_rhb_center_ent_id` int DEFAULT NULL,
  `mod_rhb_center_state` int NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `mod_rehabilitation_intern`
--

CREATE TABLE `mod_rehabilitation_intern` (
  `mod_rhb_int_id` int NOT NULL,
  `mod_rhb_int_acu_id` int NOT NULL,
  `mod_rhb_int_date_intern` datetime DEFAULT NULL,
  `mod_rhb_int_date_graduation` datetime DEFAULT NULL,
  `mod_rhb_int_date_leaving` datetime DEFAULT NULL,
  `mod_rhb_int_reason_leaving` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci,
  `mod_rhb_int_reason_addiction` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `mod_rhb_int_primary_consumption` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `mod_rhb_int_state` int DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `mod_rehabilitation_primary_consumption`
--

CREATE TABLE `mod_rehabilitation_primary_consumption` (
  `mod_rhb_pcn_id` int NOT NULL,
  `mod_rhb_pcn_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `mod_rhb_pcn_summary` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `mod_rhb_pcn_path` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `mod_rhb_pcn_order` int DEFAULT NULL,
  `mod_rhb_pcn_state` int NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `mod_rehabilitation_tutor`
--

CREATE TABLE `mod_rehabilitation_tutor` (
  `mod_rhb_tutor_id` int NOT NULL,
  `mod_rhb_tutor_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `mod_rhb_tutor_fathers_lastname` varchar(250) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `mod_rhb_tutor_mothers_lastname` varchar(250) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `mod_rhb_tutor_rhb_id` int DEFAULT NULL COMMENT 'mod_rhb_user_id',
  `mod_rhb_tutor_gender` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `mod_rhb_tutor_ci` int DEFAULT NULL,
  `mod_rhb_tutor_ext` varchar(2) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `mod_rhb_tutor_dial` varchar(5) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `mod_rhb_tutor_celular` int DEFAULT NULL,
  `mod_rhb_tutor_relationship` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `mod_rhb_tutor_address` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `mod_rhb_tutor_intern_ids` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `mod_rhb_tutor_coordinates` varchar(450) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `mod_rhb_tutor_state` int NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `mod_rehabilitation_user`
--

CREATE TABLE `mod_rehabilitation_user` (
  `mod_rhb_id` int NOT NULL,
  `mod_rhb_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `mod_rhb_lastname` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `mod_rhb_acu_id` int DEFAULT NULL,
  `mod_rhb_tepcyr` varchar(11) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT 'Time from First Consumption to Rehabilitation',
  `mod_rhb_birthday` date DEFAULT NULL,
  `mod_rhb_nationality` varchar(550) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `mod_rhb_age_first_use` int DEFAULT NULL,
  `mod_rhb_register_date` date DEFAULT NULL,
  `mod_rhb_user_id` int DEFAULT NULL,
  `mod_rhb_state` int NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `mod_rewards`
--

CREATE TABLE `mod_rewards` (
  `mod_rwd_id` int NOT NULL,
  `mod_rwd_cpa_id` int NOT NULL,
  `mod_rwd_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `mod_rwd_img` int DEFAULT NULL,
  `mod_rwd_description` tinytext CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci,
  `mod_rwd_code` varchar(21) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `mod_rwd_value` int NOT NULL DEFAULT '0',
  `mod_rwd_json` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `mod_rwd_register_date` datetime NOT NULL,
  `mod_rwd_user_id` int DEFAULT NULL,
  `mod_rwd_state` int NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `mod_rewards_register`
--

CREATE TABLE `mod_rewards_register` (
  `mod_rwd_reg_rwd_id` int NOT NULL,
  `mod_rwd_reg_acu_id` int NOT NULL,
  `mod_rwd_reg_cpa_id` int NOT NULL,
  `mod_rwd_reg_register_date` datetime NOT NULL,
  `mod_rwd_state` int NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `mod_services`
--

CREATE TABLE `mod_services` (
  `mod_sv_id` int NOT NULL,
  `mod_sv_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `mod_sv_description` varchar(550) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `mod_sv_parent_id` int NOT NULL,
  `mod_sv_state` int DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `mod_sliders`
--

CREATE TABLE `mod_sliders` (
  `mod_sli_id` int NOT NULL,
  `mod_sli_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `mod_sli_description` tinytext CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `mod_sli_cls` varchar(45) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `mod_sli_json` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci,
  `mod_sli_ent_id` int NOT NULL,
  `mod_sli_state` int NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `mod_sliders_files`
--

CREATE TABLE `mod_sliders_files` (
  `mod_sli_file_file_id` int NOT NULL,
  `mod_sli_file_sli_id` int NOT NULL,
  `mod_sli_file_description` tinytext CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci,
  `mod_sli_file_order` int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `mod_sports`
--

CREATE TABLE `mod_sports` (
  `mod_spt_id` int NOT NULL,
  `mod_spt_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `mod_sp_path` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `mod_spt_description` mediumtext CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci,
  `mod_spt_img` int DEFAULT NULL,
  `mod_spt_ent_id` int DEFAULT NULL,
  `mod_spt_state` int DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `mod_sports_dates`
--

CREATE TABLE `mod_sports_dates` (
  `mod_sp_dt_id` int NOT NULL,
  `mod_sp_dt_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `mod_sp_dt_date` date DEFAULT NULL,
  `mod_sp_dt_comp_id` int DEFAULT NULL COMMENT 'competition',
  `mod_sp_dt_div_id` int DEFAULT NULL,
  `mod_sp_dt_ent_id` int DEFAULT NULL,
  `mod_sp_dt_state` int NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `mod_sports_games`
--

CREATE TABLE `mod_sports_games` (
  `mod_sp_gm_id` int NOT NULL,
  `mod_sp_gm_tm_1` int DEFAULT NULL,
  `mod_sp_gm_tm_2` int DEFAULT NULL,
  `mod_sp_gm_place` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `mod_sp_gm_info` int DEFAULT NULL,
  `mod_sp_gm_tm_score_1` int DEFAULT NULL,
  `mod_sp_gm_tm_score_2` int DEFAULT NULL,
  `mod_sg_gm_status` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `mod_sg_gm_date` datetime DEFAULT NULL,
  `mod_sg_gm_link` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `mod_sg_gm_state` int NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `mod_sports_teams`
--

CREATE TABLE `mod_sports_teams` (
  `mod_sp_tm_id` int NOT NULL,
  `mod_sp_tm_sport` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `mod_sp_tm_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `mod_sp_tm_pathurl` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `mod_sp_tm_details` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci,
  `mod_sp_tm_logo` int DEFAULT NULL COMMENT 'file ID',
  `mod_sp_tm_banner` int DEFAULT NULL COMMENT 'file Id',
  `mod_sp_tm_json` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci,
  `mos_sp_tm_ent_id` int DEFAULT NULL,
  `mod_sp_tm_state` int NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `mod_stock`
--

CREATE TABLE `mod_stock` (
  `mod_stk_id` int NOT NULL,
  `mod_stk_sku` varchar(22) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `mod_stk_prod_id` int NOT NULL,
  `mod_stk_quantity` int DEFAULT NULL,
  `mod_stk_register_date` datetime NOT NULL,
  `mod_stk_comment` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `mod_stk_inv_id` int DEFAULT NULL COMMENT 'id factura',
  `mod_stk_max` int DEFAULT NULL,
  `mod_stk_min` int DEFAULT NULL,
  `mod_stk_alert` int DEFAULT NULL,
  `mod_stk_receipt_date` datetime NOT NULL COMMENT 'fecha de recepción',
  `mod_stk_lote` int NOT NULL,
  `mod_stk_prv_id` int NOT NULL COMMENT 'id proveedor',
  `mod_stk_net_price` decimal(20,2) NOT NULL DEFAULT '0.00' COMMENT 'precio neto',
  `mod_stk_ent_id` int DEFAULT NULL,
  `mod_stk_state` int NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `mod_stock_receipt`
--

CREATE TABLE `mod_stock_receipt` (
  `mod_str_id` int NOT NULL,
  `mod_str_prod_id` int DEFAULT NULL,
  `mod_str_quantity` int DEFAULT NULL,
  `mod_str_register_date` datetime NOT NULL,
  `mod_str_comment` mediumtext CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `mod_str_inv_id` int DEFAULT NULL COMMENT 'id factura',
  `mod_str_max` int DEFAULT NULL,
  `mod_str_min` int DEFAULT NULL,
  `mod_str_receipt_date` datetime NOT NULL COMMENT 'fecha de recepción',
  `mod_str_lote` int NOT NULL,
  `mod_str_prv_id` int NOT NULL COMMENT 'id proveedor',
  `mod_str_net_price` decimal(20,2) NOT NULL,
  `mod_str_cash_discount` decimal(20,2) NOT NULL,
  `mod_str_discount` int NOT NULL,
  `mod_str_state` int NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `mod_syllabus`
--

CREATE TABLE `mod_syllabus` (
  `mod_syl_id` int NOT NULL,
  `mod_syl_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `mod_syl_summary` varchar(455) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `mod_syl_ent_id` int DEFAULT NULL,
  `mod_syl_state` int NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `mod_syllabus_categorys`
--

CREATE TABLE `mod_syllabus_categorys` (
  `mod_syl_cat_syl_id` int NOT NULL,
  `mod_syl_cat_cat_id` int NOT NULL,
  `mod_syl_cat_order` int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `mod_syllabus_contents`
--

CREATE TABLE `mod_syllabus_contents` (
  `mod_syl_cont_id` int NOT NULL,
  `mod_syl_cont_title` varchar(445) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `mod_syl_cont_summary` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci,
  `mod_syl_cont_per_id` int DEFAULT NULL,
  `mod_syl_cont_syl_id` int DEFAULT NULL,
  `mod_syl_cont_order` int DEFAULT NULL,
  `mod_syl_cont_state` int NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `mod_syllabus_periods`
--

CREATE TABLE `mod_syllabus_periods` (
  `mod_syl_per_id` int NOT NULL,
  `mod_syl_per_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `mod_syl_per_summary` mediumtext CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci,
  `mod_syl_per_ent_id` int DEFAULT NULL,
  `mod_syl_per_state` int NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `mod_syllabus_relations_periods`
--

CREATE TABLE `mod_syllabus_relations_periods` (
  `mod_syl_rel_per_syl_id` int NOT NULL,
  `mod_syl_rel_per_per_id` int NOT NULL,
  `mod_syl_rel_per_order` int DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `mod_warehouses`
--

CREATE TABLE `mod_warehouses` (
  `mod_wrh_id` int NOT NULL,
  `mod_wrh_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `mod_wrh_details` tinytext CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci,
  `mod_wrh_primary` bit(2) DEFAULT b'0',
  `mod_wrh_state` int NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Volcado de datos para la tabla `mod_warehouses`
--

INSERT INTO `mod_warehouses` (`mod_wrh_id`, `mod_wrh_name`, `mod_wrh_details`, `mod_wrh_primary`, `mod_wrh_state`) VALUES
(1, 'Almacen General', NULL, b'01', 1),
(2, 'Almacen 2', NULL, b'00', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `notifications`
--

CREATE TABLE `notifications` (
  `ntf_id` int NOT NULL,
  `ntf_type` varchar(120) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `ntf_emitter_id` int DEFAULT NULL,
  `ntf_receiver_id` int DEFAULT NULL,
  `ntf_message` varchar(450) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `ntf_register_date` datetime DEFAULT NULL,
  `ntf_json` varchar(450) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `ntf_state` int DEFAULT '0' COMMENT '0 delete, 1 register, 2 recieved 3 read\r\n4. no recived 5.cancel\r\n'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `options`
--

CREATE TABLE `options` (
  `option_id` bigint UNSIGNED NOT NULL,
  `option_name` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '',
  `option_value` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `autoload` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT 'yes'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Volcado de datos para la tabla `options`
--

INSERT INTO `options` (`option_id`, `option_name`, `option_value`, `autoload`) VALUES
(1, 'site_title', 'Ion', 'yes'),
(2, 'site_favicon', 'assets/img/favicon.png', 'yes'),
(3, 'site_img', 'assets/img/logo.svg', 'yes'),
(4, 'site_version', '22662', 'yes'),
(5, 'user_id_default', '3', 'yes'),
(6, 'bearer_token', 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiI3MjM0OTIzODczNDkiLCJuYW1lIjoibnVjbGVvIiwiaWF0Ijo5MzI0MjQzNDl9.jIsiHIdOGa-KHCJ2mUhLLKqlvTYVLolUyrp1HM6E7Cs-gedeon', 'yes'),
(7, 'client_id', 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9-gedeon', 'yes'),
(8, 'client_secret', 'eyJzdWIiOiI3MjM0OTIzODczNDkiLCJuYW1lIjoibnVjbGVvIiwiaWF0Ijo5MzI0MjQzNDl9-gedeon', 'yes'),
(9, 'locale', 'es_ES', 'yes'),
(10, 'timezone', 'America/La_Paz', 'yes'),
(12, 'site-meta', 'sites/default/controllers/pub/meta.pub.php', 'yes'),
(13, 'entitie_id_default', '1', 'yes'),
(14, 'dashboard_img', 'assets/img/logo.svg', 'yes'),
(15, 'dashboard_favicon', 'assets/img/favicon.png', 'yes'),
(16, 'dashboard_brand', 'assets/img/logo.svg', 'yes'),
(17, 'path_icons_pub', 'assets/img/pubs', 'yes');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `posts`
--

CREATE TABLE `posts` (
  `post_id` int NOT NULL,
  `post_title` varchar(455) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `post_pathurl` varchar(455) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `post_tags` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `post_review` tinytext CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci,
  `post_img` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT '0',
  `post_img_ref` varchar(445) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `post_embed` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci,
  `post_preview_embed` int DEFAULT '0',
  `post_intern_embed` int DEFAULT '0',
  `post_video` int DEFAULT '0',
  `post_preview_video` int DEFAULT '0',
  `post_intern_video` int DEFAULT '0',
  `post_body` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `post_place` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `post_author` int DEFAULT NULL,
  `post_cls` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `post_top_relations` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `post_middle_relations` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `post_last_relations` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `post_json` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci,
  `post_date` datetime DEFAULT NULL,
  `post_register_date` datetime NOT NULL,
  `post_ent_id` int DEFAULT NULL,
  `post_user_id` int DEFAULT NULL,
  `post_state` int DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `posts_authors`
--

CREATE TABLE `posts_authors` (
  `post_au_id` int NOT NULL,
  `post_au_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `post_au_summary` tinytext CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci,
  `post_au_img` int DEFAULT '0',
  `post_au_ent_id` int DEFAULT NULL,
  `post_au_contact` tinytext CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci,
  `post_au_path` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `post_au_state` int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `posts_categorys`
--

CREATE TABLE `posts_categorys` (
  `post_cat_post_id` int NOT NULL,
  `post_cat_cat_id` int NOT NULL,
  `post_cat_ent_id` int DEFAULT NULL,
  `post_cat_order` int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `posts_files`
--

CREATE TABLE `posts_files` (
  `post_file_post_id` int NOT NULL,
  `post_file_file_id` int NOT NULL,
  `post_file_order` int DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `publications`
--

CREATE TABLE `publications` (
  `pub_id` int NOT NULL,
  `pub_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `pub_description` mediumtext CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci,
  `pub_title` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `pub_summary` mediumtext CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `pub_path` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `pub_path_ui` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `pub_path_icon` varchar(455) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `pub_icon` varchar(12) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `pub_type` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `pub_class` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `pub_attr_id` varchar(455) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `pub_attr` varchar(445) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `pub_count` int DEFAULT NULL,
  `pub_json` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci,
  `pub_state` int DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `publications_pattern`
--

CREATE TABLE `publications_pattern` (
  `pub_pat_id` int NOT NULL,
  `pub_pat_name` varchar(250) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `pub_pat_description` tinytext CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `pub_pat_json` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `pub_pat_ent_id` int DEFAULT '0',
  `pub_pat_order` int DEFAULT '0',
  `pub_pat_state` int NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `publications_relations`
--

CREATE TABLE `publications_relations` (
  `pub_rel_id` int NOT NULL,
  `pub_rel_cat_id` int DEFAULT NULL,
  `pub_rel_ws_id` int DEFAULT NULL,
  `pub_rel_block_id` int DEFAULT NULL,
  `pub_rel_pub_id` int DEFAULT NULL,
  `pub_rel_state` int NOT NULL DEFAULT '1',
  `pub_rel_order` int NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `roles`
--

CREATE TABLE `roles` (
  `rol_id` int NOT NULL,
  `rol_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `rol_description` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `rol_parent_id` int DEFAULT NULL,
  `rol_redirection_url` varchar(1000) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `rol_json` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci,
  `rol_state` int DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Volcado de datos para la tabla `roles`
--

INSERT INTO `roles` (`rol_id`, `rol_name`, `rol_description`, `rol_parent_id`, `rol_redirection_url`, `rol_json`, `rol_state`) VALUES
(1, 'Super Administrador', NULL, 0, '{_PATH_WEB}dashboard', NULL, 1),
(2, 'Administrador', NULL, 1, '{_PATH_WEB}dashboard', NULL, 1),
(3, 'Administrador de Entidad', NULL, 2, '{_PATH_WEB}dashboard', NULL, 1),
(4, 'Administrador RRHH', NULL, 2, '{_PATH_WEB}dashboard', NULL, 1),
(10, 'Encargado de Contabilidad', NULL, 2, '{_PATH_WEB}dashboard', NULL, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `roles_categorys`
--

CREATE TABLE `roles_categorys` (
  `rol_cat_rol_id` int NOT NULL,
  `rol_cat_cat_id` int NOT NULL,
  `rol_cat_order` int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `roles_entities`
--

CREATE TABLE `roles_entities` (
  `rol_ent_rol_id` int NOT NULL,
  `rol_ent_ent_id` int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `roles_modules`
--

CREATE TABLE `roles_modules` (
  `rol_mod_rol_id` int NOT NULL,
  `rol_mod_mod_id` int NOT NULL,
  `rol_mod_ent_id` int NOT NULL,
  `rol_mod_order` int NOT NULL,
  `rol_mod_permits` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '0,0,0,0,0' COMMENT 'See,Publish,Add,Edit,Delete'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Volcado de datos para la tabla `roles_modules`
--

INSERT INTO `roles_modules` (`rol_mod_rol_id`, `rol_mod_mod_id`, `rol_mod_ent_id`, `rol_mod_order`, `rol_mod_permits`) VALUES
(5, 500, 1, 1, '0,0,0,0,0'),
(6, 500, 1, 0, '0,0,0,0,0'),
(7, 500, 1, 0, '0,0,0,0,0'),
(5, 501, 1, 1, '0,0,0,0,0'),
(6, 501, 1, 0, '0,0,0,0,0'),
(7, 501, 1, 1, '0,0,0,0,0'),
(10, 1000, 1, 1, '0,0,0,0,0'),
(10, 1001, 1, 1, '0,0,0,0,0');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `roles_sites`
--

CREATE TABLE `roles_sites` (
  `rol_site_rol_id` int NOT NULL,
  `rol_site_site_id` int NOT NULL,
  `rol_site_order` int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `roles_systems`
--

CREATE TABLE `roles_systems` (
  `rol_sys_rol_id` int NOT NULL,
  `rol_sys_sys_id` int NOT NULL,
  `rol_sys_ent_id` int NOT NULL,
  `rol_sys_order` int DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Volcado de datos para la tabla `roles_systems`
--

INSERT INTO `roles_systems` (`rol_sys_rol_id`, `rol_sys_sys_id`, `rol_sys_ent_id`, `rol_sys_order`) VALUES
(2, 6, 1, 0),
(5, 6, 1, 1),
(6, 6, 1, 2),
(7, 6, 1, 0),
(10, 5, 1, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sites`
--

CREATE TABLE `sites` (
  `site_id` int NOT NULL,
  `site_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `site_path_web` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `site_path_host` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `site_title` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `site_head` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci,
  `site_head_path_url` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `site_default` int NOT NULL DEFAULT '0',
  `site_state` int NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sites_entities`
--

CREATE TABLE `sites_entities` (
  `site_ent_site_id` int NOT NULL,
  `site_ent_ent_id` int NOT NULL,
  `site_ent_order` int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `systems`
--

CREATE TABLE `systems` (
  `sys_id` int NOT NULL,
  `sys_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `sys_description` tinytext CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci,
  `sys_pathurl` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `sys_path` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `sys_mod_default` int NOT NULL DEFAULT '0',
  `sys_code` varchar(11) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `sys_icon` varchar(240) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `sys_color` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `sys_parent_id` int DEFAULT NULL,
  `sys_indexjs` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `sys_css` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `sys_order` int NOT NULL,
  `sys_state` int NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Volcado de datos para la tabla `systems`
--

INSERT INTO `systems` (`sys_id`, `sys_name`, `sys_description`, `sys_pathurl`, `sys_path`, `sys_mod_default`, `sys_code`, `sys_icon`, `sys_color`, `sys_parent_id`, `sys_indexjs`, `sys_css`, `sys_order`, `sys_state`) VALUES
(1, 'Websites', NULL, 'websites', 'modules/websites/', 10, '', 'icon icon-code', '#6F91E9', 0, 'components/websites.js', 'assets/css/dist/websites.css', 0, 1),
(2, 'Recursos Humanos', NULL, 'rrhh', 'modules/rrhh/', 200, '', 'icon icon-category', '#6F91E9', 0, 'components/rrhh.js', 'assets/css/dist/rrhh.css', 0, 1),
(4, 'Inventario', NULL, 'inventory', 'modules/inventory/', 400, '', 'icon icon-box', '#FEBF10', 0, 'components/inventory.js', 'assets/css/dist/inventory.css', 0, 1),
(5, 'Contabilidad', NULL, 'accounting', 'modules/accounting/', 500, '', 'icon icon-coin', '#6F91E9', 0, 'components/accounting.js', 'assets/css/dist/accounting.css', 0, 1),
(6, 'Consejeria', NULL, 'counseling', 'modules/counseling/', 600, '', 'icon icon-users', '#27d27c', 0, 'components/counseling.js', 'assets/css/dist/counseling.css', 0, 1),
(7, 'Ventas', NULL, 'sales', 'modules/sales/', 700, '', 'icon icon-sales', '#fc9835', 0, 'components/sales.js', 'assets/css/dist/sales.css', 0, 0),
(8, 'Brokers', NULL, 'brokers', 'modules/brokers/', 800, '', 'icon icon-circle-doble', '#27d27c', 0, 'components/brokers.js', 'assets/css/dist/brokers.css', 0, 0),
(10, 'Suscriptiones', NULL, 'suscriptions', 'modules/accounts/', 1000, '', 'icon icon-tag', '#1be5fc', 0, 'components/subscriptions.js', 'assets/css/dist/subscriptions.css', 0, 1),
(12, 'Finalcial Services Leeds', NULL, 'fls', 'modules/fls/', 1200, '', 'icon icon-circle', '#27d27c', 0, 'components/fls.js', 'assets/css/dist/fls.css', 0, 0),
(13, 'Boletería', NULL, 'tickets', 'modules/tickets/', 1300, '', 'icon icon-tag', '#27d27c', 0, 'components/tickets.js', 'assets/css/dist/tickets.min.css', 0, 1),
(14, 'Rehabilitación', NULL, 'rehabilitation', 'modules/rehabilitation/', 1400, '', 'icon icon-user', '#6F91E', 0, 'components/rehabilitation.js', 'assets/css/dist/rehabilitation.min.css', 0, 1),
(15, 'Ads', NULL, 'ads', 'modules/ads/', 1500, '', 'icon icon-loudspeaker', '#e71882', 0, 'components/ads.js', 'assets/css/dist/ads.min.css', 0, 1),
(16, 'LMS', NULL, 'lms', 'modules/lms/', 1600, '', 'icon icon-graduation', '#FEBF10', 0, 'components/lms.js', 'assets/css/dist/lms.min.css', 0, 1),
(17, 'Activo Fijo', NULL, 'fixedassets', 'modules/fixedassets/', 1700, '', 'icon icon-fixed-assets', '#9c27b0', 0, 'components/fixedassets.js', 'assets/css/dist/fixedassets.min.css', 0, 1),
(18, 'Operaciones', NULL, 'operations', 'modules/operations/', 1800, '', 'icon icon-operations', '#258dfc', 0, 'components/operations.js', 'assets/css/dist/operations.min.css', 0, 1),
(19, 'CRM', NULL, 'crm', 'modules/crm/', 1900, '', 'icon icon-crm', '#ef4848', 0, 'components/crm.js', 'assets/css/dist/crm.min.css', 0, 1),
(20, 'Restaurant', NULL, 'restaurants', 'modules/restaurants/', 2000, '', 'icon icon-restaurant', '#6b6fa9', 0, 'components/restaurants.js', 'assets/css/dist/restaurants.min.css', 0, 1),
(21, 'Doc. Externos', NULL, 'externaldocuments', 'modules/externaldocuments/', 2100, '', 'icon icon-doc', '#6b6fa9', 0, 'components/externaldocuments.js', 'assets/css/dist/externaldocuments.min.css', 0, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `systems_modules`
--

CREATE TABLE `systems_modules` (
  `sys_mod_sys_id` int NOT NULL,
  `sys_mod_mod_id` int NOT NULL,
  `sys_mod_ent_id` int NOT NULL,
  `sys_mod_order` int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Volcado de datos para la tabla `systems_modules`
--

INSERT INTO `systems_modules` (`sys_mod_sys_id`, `sys_mod_mod_id`, `sys_mod_ent_id`, `sys_mod_order`) VALUES
(1, 10, 1, 1),
(1, 11, 1, 2),
(1, 12, 1, 3),
(1, 13, 1, 4),
(1, 14, 1, 5),
(1, 15, 1, 6),
(1, 16, 1, 7),
(1, 17, 1, 8),
(1, 18, 1, 9),
(2, 200, 1, 1),
(2, 210, 1, 2),
(2, 220, 1, 3),
(4, 400, 1, 1),
(4, 401, 1, 2),
(4, 402, 1, 1),
(4, 403, 1, 2),
(5, 500, 1, 1),
(6, 600, 1, 1),
(6, 601, 1, 2),
(6, 602, 1, 3),
(6, 603, 1, 4),
(7, 700, 1, 1),
(7, 701, 1, 2),
(8, 800, 1, 1),
(8, 801, 1, 2),
(10, 1000, 1, 1),
(10, 1001, 1, 2),
(10, 1002, 1, 3),
(12, 1200, 1, 1),
(12, 1201, 1, 2),
(13, 1300, 1, 1),
(13, 1301, 1, 2),
(14, 1400, 1, 1),
(14, 1401, 1, 2),
(15, 1500, 1, 1),
(15, 1501, 1, 2),
(15, 1502, 1, 3),
(16, 1600, 1, 1),
(16, 1601, 1, 2),
(17, 1700, 1, 1),
(17, 1701, 1, 2),
(18, 1800, 1, 1),
(19, 1900, 1, 1),
(19, 1901, 1, 2),
(20, 2000, 1, 1),
(20, 2001, 1, 2),
(21, 2100, 1, 1),
(21, 2101, 1, 2),
(21, 2102, 1, 2),
(21, 2103, 1, 2);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `users`
--

CREATE TABLE `users` (
  `user_id` int NOT NULL,
  `user_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `user_lastname` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `user_email` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `user_password` varchar(45) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `user_img` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `user_user` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `user_level` int NOT NULL DEFAULT '0',
  `user_state` int DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Volcado de datos para la tabla `users`
--

INSERT INTO `users` (`user_id`, `user_name`, `user_lastname`, `user_email`, `user_password`, `user_img`, `user_user`, `user_level`, `user_state`) VALUES
(1, 'admin', 'general', 'adm@wappcom.com', 'NDg2Mg==', 'https://lh3.googleusercontent.com/a-/AOh14Gg0Q2OTE5-h1lN_wfEqTXCcrB_gY0tSmuF8UIi6Kw=s96-c', 'admin', 0, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `users_firms`
--

CREATE TABLE `users_firms` (
  `user_firm_id` int NOT NULL,
  `user_firm_user_id` int NOT NULL,
  `user_firm_img` varchar(455) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `user_firm_reference` int DEFAULT NULL,
  `user_firm_md5` int DEFAULT NULL,
  `user_firm_state` int NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `users_groups`
--

CREATE TABLE `users_groups` (
  `user_group_user_id` int NOT NULL,
  `user_group_group_id` int NOT NULL,
  `user_group_ent_id` int NOT NULL,
  `user_group_order` int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `users_path`
--

CREATE TABLE `users_path` (
  `user_path_user_id` int NOT NULL,
  `user_path_mod_id` int NOT NULL,
  `user_path_path` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `user_path_order` int NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `users_roles`
--

CREATE TABLE `users_roles` (
  `user_rol_user_id` int NOT NULL,
  `user_rol_rol_id` int NOT NULL,
  `user_rol_ent_id` int NOT NULL,
  `user_rol_order` int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Volcado de datos para la tabla `users_roles`
--

INSERT INTO `users_roles` (`user_rol_user_id`, `user_rol_rol_id`, `user_rol_ent_id`, `user_rol_order`) VALUES
(1, 1, 1, 0),
(3, 10, 1, 1),
(4, 10, 1, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `users_tabs`
--

CREATE TABLE `users_tabs` (
  `user_tab_user_id` int NOT NULL,
  `user_tab_sys_id` int NOT NULL,
  `user_tab_ent_id` int NOT NULL,
  `user_tab_sys_active` int NOT NULL,
  `user_tab_start_mod_id` int DEFAULT '0',
  `user_tab_json` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci,
  `user_tab_order` int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Volcado de datos para la tabla `users_tabs`
--

INSERT INTO `users_tabs` (`user_tab_user_id`, `user_tab_sys_id`, `user_tab_ent_id`, `user_tab_sys_active`, `user_tab_start_mod_id`, `user_tab_json`, `user_tab_order`) VALUES
(1, 4, 1, 0, 401, NULL, 3),
(1, 19, 1, 1, 1901, NULL, 3);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `users_tokens`
--

CREATE TABLE `users_tokens` (
  `user_tk_user_id` bigint NOT NULL,
  `user_tk_type` varchar(15) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `user_tk_token` varchar(400) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `user_tk_expires_in` datetime DEFAULT NULL,
  `user_tk_date` datetime DEFAULT NULL,
  `user_tk_dates_browser` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Volcado de datos para la tabla `users_tokens`
--

INSERT INTO `users_tokens` (`user_tk_user_id`, `user_tk_type`, `user_tk_token`, `user_tk_expires_in`, `user_tk_date`, `user_tk_dates_browser`) VALUES
(1, 'access_token', 'ZXlKaGJHY2lPaUpJVXpJMU5pSXNJblI1Y0NJNklrcFhWQ0o5LWdlZGVvbi4xLjEuMC40MDYyNTMwMCAxNzAwOTQ1OTM3NjU2MjYwMTE2MzJmNzkuOTYyNjUyOTc=', NULL, '2023-11-25 16:58:57', 'Google Chrome,118.0.0.0,Unknown:Mozilla/5.0 (X11; CrOS x86_64 14541.0.0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/118.0.0.0 Safari/537.36'),
(1, 'access_token', 'ZXlKaGJHY2lPaUpJVXpJMU5pSXNJblI1Y0NJNklrcFhWQ0o5LWdlZGVvbi4xLjEuMC41NjIzOTYwMCAxNjg3NDc5NTIzNjQ5NGU0ZTM4OTRlMjMuNjIyODkyMjg=', NULL, '2023-06-22 20:18:43', 'Google Chrome,114.0.0.0,Unknown:Mozilla/5.0 (X11; CrOS x86_64 14541.0.0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36'),
(1, 'access_token', 'ZXlKaGJHY2lPaUpJVXpJMU5pSXNJblI1Y0NJNklrcFhWQ0o5LWdlZGVvbi4xLjEuMC42MTA4MDEwMCAxNjk2ODc5NTYwNjUyNDUzYzg5NTFmOTMuNTUxNzMxMDk=', NULL, '2023-10-09 15:26:00', 'Google Chrome,117.0.0.0,Mac Os:Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
(1, 'access_token', 'ZXlKaGJHY2lPaUpJVXpJMU5pSXNJblI1Y0NJNklrcFhWQ0o5LWdlZGVvbi4xLjEuMC42MzMzMTUwMCAxNjk1MTI4MzQ4NjUwOTliMWM5YTllZTMuOTU4MDUyMTY=', NULL, '2023-09-19 08:59:08', 'Google Chrome,116.0.0.0,Mac Os:Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/116.0.0.0 Safari/537.36'),
(1, 'access_token', 'ZXlKaGJHY2lPaUpJVXpJMU5pSXNJblI1Y0NJNklrcFhWQ0o5LWdlZGVvbi4xLjEuMC42NjExNjIwMCAxNzA0NTU4MTYzNjU5OTdlNTNhMTZiNzMuMjIzNDc0NDg=', NULL, '2024-01-06 12:22:43', 'Google Chrome,120.0.0.0,Mac Os:Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/120.0.0.0 Safari/537.36'),
(1, 'access_token', 'ZXlKaGJHY2lPaUpJVXpJMU5pSXNJblI1Y0NJNklrcFhWQ0o5LWdlZGVvbi4xLjEuMC43MDI0OTIwMCAxNzA3MzE1MTYwNjVjMzhmZDhhYjgzMTMuMjM5MTE3NDg=', NULL, '2024-02-07 10:12:40', 'Google Chrome,121.0.0.0,Mac Os:Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/121.0.0.0 Safari/537.36'),
(1, 'access_token', 'ZXlKaGJHY2lPaUpJVXpJMU5pSXNJblI1Y0NJNklrcFhWQ0o5LWdlZGVvbi4xLjEuMC43NDQ0ODkwMCAxNzA1NjM2OTYzNjVhOWY0NjNiNWMzMTAuNjY1NTEzMzE=', NULL, '2024-01-19 00:02:43', 'Google Chrome,120.0.0.0,Mac Os:Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/120.0.0.0 Safari/537.36'),
(1, 'access_token', 'ZXlKaGJHY2lPaUpJVXpJMU5pSXNJblI1Y0NJNklrcFhWQ0o5LWdlZGVvbi4xLjEuMC43Njg2OTYwMCAxNjk5Njc1Mjc5NjU0ZWZjOGZiYmFjMDcuNDkyMTgzNzk=', NULL, '2023-11-11 00:01:19', 'Google Chrome,119.0.0.0,Linux:Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/119.0.0.0 Safari/537.36'),
(1, 'access_token', 'ZXlKaGJHY2lPaUpJVXpJMU5pSXNJblI1Y0NJNklrcFhWQ0o5LWdlZGVvbi4xLjEuMC44MzQxMDYwMCAxNjk4NTg4OTY3NjUzZTY5MjdjYmE0MzMuNjQ4MDc5MzY=', NULL, '2023-10-29 10:16:07', 'Google Chrome,118.0.0.0,Mac Os:Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/118.0.0.0 Safari/537.36'),
(1, 'access_token', 'ZXlKaGJHY2lPaUpJVXpJMU5pSXNJblI1Y0NJNklrcFhWQ0o5LWdlZGVvbi4xLjEuMC45Mjk1NzkwMCAxNjk1MjEzNTcxNjUwYWU4MDNlMmYzODQuNDE3OTQzMjA=', NULL, '2023-09-20 08:39:31', 'Google Chrome,116.0.0.0,Mac Os:Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/116.0.0.0 Safari/537.36'),
(1, 'access_token', 'ZXlKaGJHY2lPaUpJVXpJMU5pSXNJblI1Y0NJNklrcFhWQ0o5LWdlZGVvbi4xLjEuMC45NDcxODYwMCAxNzAwOTYwODU0NjU2MjlhNTZlNzNmNjguNDQzNDUzMzk=', NULL, '2023-11-25 21:07:34', 'Google Chrome,119.0.0.0,Mac Os:Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/119.0.0.0 Safari/537.36'),
(1, 'access_token', 'ZXlKaGJHY2lPaUpJVXpJMU5pSXNJblI1Y0NJNklrcFhWQ0o5LWdlZGVvbi4xLjEuMC4xNDM0MzgwMCAxNzA1NjI3MzYwNjVhOWNlZTAyMzA1OTAuOTQ2NTE5MTE=', NULL, '2024-01-18 21:22:40', 'Google Chrome,120.0.0.0,Mac Os:Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/120.0.0.0 Safari/537.36'),
(1, 'access_token', 'ZXlKaGJHY2lPaUpJVXpJMU5pSXNJblI1Y0NJNklrcFhWQ0o5LWdlZGVvbi4xLjEuMC4yMjU3MzAwMCAxNzAwMDAwODY1NjU1M2Y0NjEzNzFjZjMuMjMzMDYxODI=', NULL, '2023-11-14 18:27:45', 'Google Chrome,119.0.0.0,Mac Os:Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/119.0.0.0 Safari/537.36'),
(1, 'access_token', 'ZXlKaGJHY2lPaUpJVXpJMU5pSXNJblI1Y0NJNklrcFhWQ0o5LWdlZGVvbi4xLjEuMC4yNzkyNjUwMCAxNjk0MDI5MDYxNjRmOGQ1MDU0NDJlOTYuNDg1NjczODY=', NULL, '2023-09-06 15:37:41', 'Google Chrome,116.0.0.0,Mac Os:Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/116.0.0.0 Safari/537.36'),
(1, 'access_token', 'ZXlKaGJHY2lPaUpJVXpJMU5pSXNJblI1Y0NJNklrcFhWQ0o5LWdlZGVvbi4xLjEuMC4yODkyNzEwMCAxNjk2ODc5NjczNjUyNDU0Mzk0NmEwMzIuNzIzNTA0ODA=', NULL, '2023-10-09 15:27:53', 'Google Chrome,117.0.0.0,Mac Os:Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
(1, 'access_token', 'ZXlKaGJHY2lPaUpJVXpJMU5pSXNJblI1Y0NJNklrcFhWQ0o5LWdlZGVvbi4xLjEuMC4zMTMyNDEwMCAxNjk2OTc3MjM5NjUyNWQxNTc0Yzc5ZDQuMDA4MDc0MTY=', NULL, '2023-10-10 18:33:59', 'Google Chrome,117.0.0.0,Mac Os:Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
(1, 'refresh_token', '1178822b70eac2b587fc6bddf9365a0f', NULL, '2023-10-29 10:16:07', 'Google Chrome,118.0.0.0,Mac Os:Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/118.0.0.0 Safari/537.36'),
(1, 'refresh_token', '1a187b8aab233304468cb3aaf183cdff', NULL, '2023-09-06 15:37:41', 'Google Chrome,116.0.0.0,Mac Os:Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/116.0.0.0 Safari/537.36'),
(1, 'refresh_token', '37aea213d4edd8a5f7bdf4f61f16797c', NULL, '2023-11-25 16:58:57', 'Google Chrome,118.0.0.0,Unknown:Mozilla/5.0 (X11; CrOS x86_64 14541.0.0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/118.0.0.0 Safari/537.36'),
(1, 'refresh_token', '485ded83fff4cf201224d19d282ce60f', NULL, '2024-01-18 21:22:40', 'Google Chrome,120.0.0.0,Mac Os:Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/120.0.0.0 Safari/537.36'),
(1, 'refresh_token', '4aba93b0ee90642ab84f26baf5c3d5d6', NULL, '2023-11-25 21:07:34', 'Google Chrome,119.0.0.0,Mac Os:Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/119.0.0.0 Safari/537.36'),
(1, 'refresh_token', '58c3c629ae9eb784eda17f9a07f55b11', NULL, '2024-01-06 12:22:43', 'Google Chrome,120.0.0.0,Mac Os:Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/120.0.0.0 Safari/537.36'),
(1, 'refresh_token', '64f3b152db7ba3c5e543bc9f50907c3c', NULL, '2023-11-14 18:27:45', 'Google Chrome,119.0.0.0,Mac Os:Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/119.0.0.0 Safari/537.36'),
(1, 'refresh_token', '775766aa9d488a19fb54c6a14463a7b0', NULL, '2024-02-07 10:12:40', 'Google Chrome,121.0.0.0,Mac Os:Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/121.0.0.0 Safari/537.36'),
(1, 'refresh_token', '87afd590e605fce463dd702e5aa2b0c9', NULL, '2023-11-11 00:01:19', 'Google Chrome,119.0.0.0,Linux:Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/119.0.0.0 Safari/537.36'),
(1, 'refresh_token', '955b353d297703fc8d7b67b83176328f', NULL, '2024-01-19 00:02:43', 'Google Chrome,120.0.0.0,Mac Os:Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/120.0.0.0 Safari/537.36'),
(1, 'refresh_token', 'b0d084adb2db94807bd4e515a2a8ff23', NULL, '2023-10-09 15:27:53', 'Google Chrome,117.0.0.0,Mac Os:Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
(1, 'refresh_token', 'bdd004e7b2938f7011577285adbbdde6', NULL, '2023-06-22 20:18:43', 'Google Chrome,114.0.0.0,Unknown:Mozilla/5.0 (X11; CrOS x86_64 14541.0.0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36'),
(1, 'refresh_token', 'c43236de53ba6db22f66bea24ee23cc0', NULL, '2023-10-10 18:33:59', 'Google Chrome,117.0.0.0,Mac Os:Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
(1, 'refresh_token', 'e155e922c8fe20906e25a8d06da1e3f6', NULL, '2023-10-09 15:26:00', 'Google Chrome,117.0.0.0,Mac Os:Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
(1, 'refresh_token', 'e5bfc8addfd273f664352fb4c9026eb2', NULL, '2023-09-20 08:39:31', 'Google Chrome,116.0.0.0,Mac Os:Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/116.0.0.0 Safari/537.36'),
(1, 'refresh_token', 'f6d110ff5a90256b0ccea049ca856b93', NULL, '2023-09-19 08:59:08', 'Google Chrome,116.0.0.0,Mac Os:Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/116.0.0.0 Safari/537.36');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `worksheets`
--

CREATE TABLE `worksheets` (
  `ws_id` int NOT NULL,
  `ws_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `ws_level` int DEFAULT NULL,
  `ws_class` varchar(44) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `ws_ent_id` int NOT NULL DEFAULT '0',
  `ws_state` int DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Volcado de datos para la tabla `worksheets`
--

INSERT INTO `worksheets` (`ws_id`, `ws_name`, `ws_level`, `ws_class`, `ws_ent_id`, `ws_state`) VALUES
(1, 'planilla 1', 1, 'ws1', 1, 1),
(2, 'planilla 2', 2, 'ws2', 1, 1),
(3, 'planilla 3', 3, 'ws3', 1, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `worksheets_blocks`
--

CREATE TABLE `worksheets_blocks` (
  `ws_block_ws_id` int NOT NULL,
  `ws_block_block_id` int NOT NULL,
  `ws_block_order` int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Volcado de datos para la tabla `worksheets_blocks`
--

INSERT INTO `worksheets_blocks` (`ws_block_ws_id`, `ws_block_block_id`, `ws_block_order`) VALUES
(1, 1, 1),
(1, 2, 2),
(1, 3, 3);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `blocks`
--
ALTER TABLE `blocks`
  ADD PRIMARY KEY (`block_id`) USING BTREE;

--
-- Indices de la tabla `categorys`
--
ALTER TABLE `categorys`
  ADD PRIMARY KEY (`cat_id`) USING BTREE;

--
-- Indices de la tabla `categorys_files`
--
ALTER TABLE `categorys_files`
  ADD PRIMARY KEY (`cat_file_file_id`,`cat_file_cat_id`) USING BTREE;

--
-- Indices de la tabla `contents`
--
ALTER TABLE `contents`
  ADD PRIMARY KEY (`cont_id`) USING BTREE;

--
-- Indices de la tabla `contents_categorys`
--
ALTER TABLE `contents_categorys`
  ADD PRIMARY KEY (`cont_cat_cont_id`,`cont_cat_cat_id`) USING BTREE;

--
-- Indices de la tabla `contents_files`
--
ALTER TABLE `contents_files`
  ADD PRIMARY KEY (`cont_file_cont_id`,`cont_file_file_id`) USING BTREE;

--
-- Indices de la tabla `contents_pubs`
--
ALTER TABLE `contents_pubs`
  ADD PRIMARY KEY (`cont_pub_cont_id`,`cont_pub_pub_id`) USING BTREE;

--
-- Indices de la tabla `docs`
--
ALTER TABLE `docs`
  ADD PRIMARY KEY (`doc_id`) USING BTREE;

--
-- Indices de la tabla `docs_categorys`
--
ALTER TABLE `docs_categorys`
  ADD PRIMARY KEY (`doc_cat_doc_id`,`doc_cat_cat_id`) USING BTREE;

--
-- Indices de la tabla `entities`
--
ALTER TABLE `entities`
  ADD PRIMARY KEY (`ent_id`) USING BTREE;

--
-- Indices de la tabla `files`
--
ALTER TABLE `files`
  ADD PRIMARY KEY (`file_id`) USING BTREE;

--
-- Indices de la tabla `groups`
--
ALTER TABLE `groups`
  ADD PRIMARY KEY (`group_id`) USING BTREE;

--
-- Indices de la tabla `groups_files`
--
ALTER TABLE `groups_files`
  ADD PRIMARY KEY (`group_file_group_id`,`group_file_file_id`) USING BTREE;

--
-- Indices de la tabla `links`
--
ALTER TABLE `links`
  ADD PRIMARY KEY (`lnk_id`) USING BTREE;

--
-- Indices de la tabla `links_categorys`
--
ALTER TABLE `links_categorys`
  ADD PRIMARY KEY (`lnk_cat_cat_id`,`lnk_cat_lnk_id`) USING BTREE;

--
-- Indices de la tabla `links_pubs`
--
ALTER TABLE `links_pubs`
  ADD PRIMARY KEY (`lnk_pub_pub_id`,`lnk_pub_lnk_id`) USING BTREE;

--
-- Indices de la tabla `menus`
--
ALTER TABLE `menus`
  ADD PRIMARY KEY (`menu_id`) USING BTREE;

--
-- Indices de la tabla `menus_items`
--
ALTER TABLE `menus_items`
  ADD PRIMARY KEY (`menu_item_id`) USING BTREE;

--
-- Indices de la tabla `modules`
--
ALTER TABLE `modules`
  ADD PRIMARY KEY (`mod_id`) USING BTREE;

--
-- Indices de la tabla `modules_categorys`
--
ALTER TABLE `modules_categorys`
  ADD PRIMARY KEY (`mod_cat_mod_id`,`mod_cat_cat_id`,`mod_cat_ent_id`) USING BTREE;

--
-- Indices de la tabla `mod_accounting_plan`
--
ALTER TABLE `mod_accounting_plan`
  ADD PRIMARY KEY (`mod_acp_id`) USING BTREE;

--
-- Indices de la tabla `mod_accounts_addresses`
--
ALTER TABLE `mod_accounts_addresses`
  ADD PRIMARY KEY (`mod_acu_add_acu_id`,`mod_acu_add_add_id`) USING BTREE;

--
-- Indices de la tabla `mod_accounts_ads`
--
ALTER TABLE `mod_accounts_ads`
  ADD PRIMARY KEY (`mod_aca_id`,`mod_aca_cen_id`) USING BTREE;

--
-- Indices de la tabla `mod_accounts_invoices_data`
--
ALTER TABLE `mod_accounts_invoices_data`
  ADD PRIMARY KEY (`mod_acu_invd_id`,`mod_acu_invd_acu_id`,`mod_acu_invd_nit`) USING BTREE;

--
-- Indices de la tabla `mod_accounts_roles`
--
ALTER TABLE `mod_accounts_roles`
  ADD PRIMARY KEY (`mod_acu_rol_id`) USING BTREE;

--
-- Indices de la tabla `mod_accounts_services`
--
ALTER TABLE `mod_accounts_services`
  ADD PRIMARY KEY (`mod_acu_sv_sv_id`,`mod_acu_sv_acu_id`) USING BTREE;

--
-- Indices de la tabla `mod_accounts_token`
--
ALTER TABLE `mod_accounts_token`
  ADD PRIMARY KEY (`mod_atk_type`,`mod_atk_token`) USING BTREE;

--
-- Indices de la tabla `mod_accounts_users`
--
ALTER TABLE `mod_accounts_users`
  ADD PRIMARY KEY (`mod_acu_id`) USING BTREE;

--
-- Indices de la tabla `mod_accounts_users_roles`
--
ALTER TABLE `mod_accounts_users_roles`
  ADD PRIMARY KEY (`mod_acu_user_rol_acu_id`,`mod_acu_user_rol_rol_id`) USING BTREE;

--
-- Indices de la tabla `mod_actions`
--
ALTER TABLE `mod_actions`
  ADD PRIMARY KEY (`mod_act_id`) USING BTREE;

--
-- Indices de la tabla `mod_actions_register`
--
ALTER TABLE `mod_actions_register`
  ADD PRIMARY KEY (`mod_act_reg_act_id`,`mod_act_reg_cpa_id`,`mod_act_reg_aca_id`,`mod_act_reg_acu_id`,`mod_act_reg_plc_id`) USING BTREE;

--
-- Indices de la tabla `mod_acuse`
--
ALTER TABLE `mod_acuse`
  ADD PRIMARY KEY (`mod_ac_id`) USING BTREE;

--
-- Indices de la tabla `mod_addresses`
--
ALTER TABLE `mod_addresses`
  ADD PRIMARY KEY (`mod_add_id`) USING BTREE;

--
-- Indices de la tabla `mod_addresses_logistics_distribution_zone`
--
ALTER TABLE `mod_addresses_logistics_distribution_zone`
  ADD PRIMARY KEY (`mod_add_ldz_add_id`,`mod_add_ldz_ldz_id`) USING BTREE;

--
-- Indices de la tabla `mod_ads`
--
ALTER TABLE `mod_ads`
  ADD PRIMARY KEY (`mod_ads_id`) USING BTREE;

--
-- Indices de la tabla `mod_ads_customers`
--
ALTER TABLE `mod_ads_customers`
  ADD PRIMARY KEY (`mod_ads_cus_id`,`mod_ads_cus_ads_id`,`mod_ads_cus_cen_id`) USING BTREE;

--
-- Indices de la tabla `mod_ads_position`
--
ALTER TABLE `mod_ads_position`
  ADD PRIMARY KEY (`mod_ads_pos_id`) USING BTREE;

--
-- Indices de la tabla `mod_advised_calendars`
--
ALTER TABLE `mod_advised_calendars`
  ADD PRIMARY KEY (`mod_adv_cal_id`) USING BTREE;

--
-- Indices de la tabla `mod_advised_calendars_blocked`
--
ALTER TABLE `mod_advised_calendars_blocked`
  ADD PRIMARY KEY (`mod_adv_cb_id`) USING BTREE;

--
-- Indices de la tabla `mod_advised_counselors`
--
ALTER TABLE `mod_advised_counselors`
  ADD PRIMARY KEY (`mod_adv_cou_acu_id`,`mod_adv_cou_cou_id`) USING BTREE;

--
-- Indices de la tabla `mod_advised_counselors_fields`
--
ALTER TABLE `mod_advised_counselors_fields`
  ADD PRIMARY KEY (`mod_adv_fds_user_id`,`mod_adv_fds_fds_id`) USING BTREE;

--
-- Indices de la tabla `mod_advised_history`
--
ALTER TABLE `mod_advised_history`
  ADD PRIMARY KEY (`mod_adv_his_id`) USING BTREE;

--
-- Indices de la tabla `mod_advised_history_calendars`
--
ALTER TABLE `mod_advised_history_calendars`
  ADD PRIMARY KEY (`mod_adv_his_cal_cal_id`,`mod_adv_his_cal_his_id`) USING BTREE;

--
-- Indices de la tabla `mod_advised_options`
--
ALTER TABLE `mod_advised_options`
  ADD PRIMARY KEY (`mod_adv_opt_id`) USING BTREE;

--
-- Indices de la tabla `mod_advised_schedules`
--
ALTER TABLE `mod_advised_schedules`
  ADD PRIMARY KEY (`mod_adv_sch_id`) USING BTREE;

--
-- Indices de la tabla `mod_advised_types_appointments`
--
ALTER TABLE `mod_advised_types_appointments`
  ADD PRIMARY KEY (`mod_adv_tya_id`) USING BTREE;

--
-- Indices de la tabla `mod_advised_user`
--
ALTER TABLE `mod_advised_user`
  ADD PRIMARY KEY (`mod_adv_id`) USING BTREE;

--
-- Indices de la tabla `mod_brands`
--
ALTER TABLE `mod_brands`
  ADD PRIMARY KEY (`mod_brd_id`) USING BTREE;

--
-- Indices de la tabla `mod_campaigns_actions`
--
ALTER TABLE `mod_campaigns_actions`
  ADD PRIMARY KEY (`mod_cpa_act_cpa_id`,`mod_cpa_act_act_id`) USING BTREE;

--
-- Indices de la tabla `mod_campaigns_ads`
--
ALTER TABLE `mod_campaigns_ads`
  ADD PRIMARY KEY (`mod_cpa_id`) USING BTREE;

--
-- Indices de la tabla `mod_carts`
--
ALTER TABLE `mod_carts`
  ADD PRIMARY KEY (`mod_cart_id`,`mod_cart_acu_id`) USING BTREE;

--
-- Indices de la tabla `mod_carts_products`
--
ALTER TABLE `mod_carts_products`
  ADD PRIMARY KEY (`mod_cart_prod_cart_id`,`mod_cart_prod_prod_id`) USING BTREE;

--
-- Indices de la tabla `mod_classification_info`
--
ALTER TABLE `mod_classification_info`
  ADD PRIMARY KEY (`mod_clsi_id`) USING BTREE;

--
-- Indices de la tabla `mod_competitions`
--
ALTER TABLE `mod_competitions`
  ADD PRIMARY KEY (`mod_cmp_id`) USING BTREE;

--
-- Indices de la tabla `mod_competitions_accounts`
--
ALTER TABLE `mod_competitions_accounts`
  ADD PRIMARY KEY (`mod_cmp_cmp_id`,`mod_cmp_acu_id`) USING BTREE;

--
-- Indices de la tabla `mod_competitions_mira`
--
ALTER TABLE `mod_competitions_mira`
  ADD PRIMARY KEY (`mod_cmp_mira_id`) USING BTREE;

--
-- Indices de la tabla `mod_customers_data`
--
ALTER TABLE `mod_customers_data`
  ADD PRIMARY KEY (`mod_ced_id`) USING BTREE;

--
-- Indices de la tabla `mod_customers_enterprises`
--
ALTER TABLE `mod_customers_enterprises`
  ADD PRIMARY KEY (`mod_cen_id`) USING BTREE;

--
-- Indices de la tabla `mod_customers_persons`
--
ALTER TABLE `mod_customers_persons`
  ADD PRIMARY KEY (`mod_cpe_id`) USING BTREE;

--
-- Indices de la tabla `mod_departments`
--
ALTER TABLE `mod_departments`
  ADD PRIMARY KEY (`mod_dep_id`) USING BTREE;

--
-- Indices de la tabla `mod_discount`
--
ALTER TABLE `mod_discount`
  ADD PRIMARY KEY (`mod_dis_id`) USING BTREE;

--
-- Indices de la tabla `mod_discount_products`
--
ALTER TABLE `mod_discount_products`
  ADD PRIMARY KEY (`mod_dis_prod_dis_id`,`mod_dis_prod_prod_id`) USING BTREE;

--
-- Indices de la tabla `mod_events`
--
ALTER TABLE `mod_events`
  ADD PRIMARY KEY (`mod_eve_id`) USING BTREE;

--
-- Indices de la tabla `mod_events_halls`
--
ALTER TABLE `mod_events_halls`
  ADD PRIMARY KEY (`mod_eve_hall_id`,`mod_eve_hall_ent_id`) USING BTREE;

--
-- Indices de la tabla `mod_events_options`
--
ALTER TABLE `mod_events_options`
  ADD PRIMARY KEY (`mod_eve_option_id`) USING BTREE;

--
-- Indices de la tabla `mod_events_tickets`
--
ALTER TABLE `mod_events_tickets`
  ADD PRIMARY KEY (`mod_eve_tck_id`) USING BTREE;

--
-- Indices de la tabla `mod_external_docs`
--
ALTER TABLE `mod_external_docs`
  ADD PRIMARY KEY (`mod_edc_id`) USING BTREE;

--
-- Indices de la tabla `mod_external_docs_options`
--
ALTER TABLE `mod_external_docs_options`
  ADD PRIMARY KEY (`mod_edc_opt_id`) USING BTREE;

--
-- Indices de la tabla `mod_external_docs_page_design`
--
ALTER TABLE `mod_external_docs_page_design`
  ADD PRIMARY KEY (`mod_edc_pd_id`) USING BTREE;

--
-- Indices de la tabla `mod_external_docs_templates`
--
ALTER TABLE `mod_external_docs_templates`
  ADD PRIMARY KEY (`mod_edc_tpl_id`) USING BTREE;

--
-- Indices de la tabla `mod_external_docs_types`
--
ALTER TABLE `mod_external_docs_types`
  ADD PRIMARY KEY (`mod_edc_type_id`) USING BTREE;

--
-- Indices de la tabla `mod_inventory_options`
--
ALTER TABLE `mod_inventory_options`
  ADD PRIMARY KEY (`mod_inv_opt_id`) USING BTREE;

--
-- Indices de la tabla `mod_inventory_units`
--
ALTER TABLE `mod_inventory_units`
  ADD PRIMARY KEY (`mod_inv_unt_id`) USING BTREE;

--
-- Indices de la tabla `mod_jobtitle`
--
ALTER TABLE `mod_jobtitle`
  ADD PRIMARY KEY (`mod_jbt_id`) USING BTREE;

--
-- Indices de la tabla `mod_kardex`
--
ALTER TABLE `mod_kardex`
  ADD PRIMARY KEY (`mod_kdx_id`,`mod_kdx_user_id`) USING BTREE;

--
-- Indices de la tabla `mod_kardex_jobtitle`
--
ALTER TABLE `mod_kardex_jobtitle`
  ADD PRIMARY KEY (`mod_kdx_jbt_jbt_id`,`mod_kdx_jbt_kdx_id`,`mod_kdx_jbt_ent_id`) USING BTREE;

--
-- Indices de la tabla `mod_leagues`
--
ALTER TABLE `mod_leagues`
  ADD PRIMARY KEY (`mod_lg_id`) USING BTREE;

--
-- Indices de la tabla `mod_leagues_divisions`
--
ALTER TABLE `mod_leagues_divisions`
  ADD PRIMARY KEY (`mod_lg_div_id`) USING BTREE;

--
-- Indices de la tabla `mod_leagues_divisions_teams`
--
ALTER TABLE `mod_leagues_divisions_teams`
  ADD PRIMARY KEY (`mod_lg_div_tm_div_id`,`mod_lg_div_tm_tm_id`) USING BTREE;

--
-- Indices de la tabla `mod_logistics_distribution_move`
--
ALTER TABLE `mod_logistics_distribution_move`
  ADD PRIMARY KEY (`mod_log_dtz_id`) USING BTREE;

--
-- Indices de la tabla `mod_logistics_distribution_zone`
--
ALTER TABLE `mod_logistics_distribution_zone`
  ADD PRIMARY KEY (`mod_log_dtz_id`) USING BTREE;

--
-- Indices de la tabla `mod_make`
--
ALTER TABLE `mod_make`
  ADD PRIMARY KEY (`mod_mak_id`) USING BTREE;

--
-- Indices de la tabla `mod_organization_chart`
--
ALTER TABLE `mod_organization_chart`
  ADD PRIMARY KEY (`mod_orc_id`) USING BTREE;

--
-- Indices de la tabla `mod_organization_chart_rel`
--
ALTER TABLE `mod_organization_chart_rel`
  ADD PRIMARY KEY (`mod_orc_rel_orc_id`,`mod_orc_rel_user_id`) USING BTREE;

--
-- Indices de la tabla `mod_payments_tx_tickets`
--
ALTER TABLE `mod_payments_tx_tickets`
  ADD PRIMARY KEY (`mod_pay_tx_tck_id`) USING BTREE;

--
-- Indices de la tabla `mod_places`
--
ALTER TABLE `mod_places`
  ADD PRIMARY KEY (`mod_plc_id`) USING BTREE;

--
-- Indices de la tabla `mod_places_accounts_ads`
--
ALTER TABLE `mod_places_accounts_ads`
  ADD PRIMARY KEY (`mod_plc_aca_aca_id`,`mod_plc_aca_plc_id`) USING BTREE;

--
-- Indices de la tabla `mod_places_categorys`
--
ALTER TABLE `mod_places_categorys`
  ADD PRIMARY KEY (`mod_plc_cat_plc_id`,`mod_plc_cat_cat_id`) USING BTREE;

--
-- Indices de la tabla `mod_prices_list`
--
ALTER TABLE `mod_prices_list`
  ADD PRIMARY KEY (`mod_prl_id`) USING BTREE;

--
-- Indices de la tabla `mod_producers`
--
ALTER TABLE `mod_producers`
  ADD PRIMARY KEY (`mod_prd_id`) USING BTREE;

--
-- Indices de la tabla `mod_products`
--
ALTER TABLE `mod_products`
  ADD PRIMARY KEY (`mod_prod_id`) USING BTREE;
ALTER TABLE `mod_products` ADD FULLTEXT KEY `mod_prod_dates` (`mod_prod_name`,`mod_prod_pathurl`,`mod_prod_tags`,`mod_prod_code`);

--
-- Indices de la tabla `mod_products_categorys`
--
ALTER TABLE `mod_products_categorys`
  ADD PRIMARY KEY (`mod_prod_cat_prod_id`,`mod_prod_cat_cat_id`,`mod_prod_cat_ent_id`) USING BTREE;

--
-- Indices de la tabla `mod_products_clothing_store`
--
ALTER TABLE `mod_products_clothing_store`
  ADD PRIMARY KEY (`mod_prod_cst_id`,`mod_prod_cst_prod_id`) USING BTREE;

--
-- Indices de la tabla `mod_products_colors`
--
ALTER TABLE `mod_products_colors`
  ADD PRIMARY KEY (`mod_prod_color_id`,`mod_prod_color_hex`) USING BTREE;

--
-- Indices de la tabla `mod_products_files`
--
ALTER TABLE `mod_products_files`
  ADD PRIMARY KEY (`mod_prod_file_prod_id`,`mod_prod_file_file_id`) USING BTREE;

--
-- Indices de la tabla `mod_products_food`
--
ALTER TABLE `mod_products_food`
  ADD PRIMARY KEY (`mod_prod_food_id`) USING BTREE;

--
-- Indices de la tabla `mod_products_options`
--
ALTER TABLE `mod_products_options`
  ADD PRIMARY KEY (`mod_prod_opt_id`) USING BTREE;

--
-- Indices de la tabla `mod_products_pharmacy`
--
ALTER TABLE `mod_products_pharmacy`
  ADD PRIMARY KEY (`mod_prod_pha_id`) USING BTREE;

--
-- Indices de la tabla `mod_products_pharmacy_stock`
--
ALTER TABLE `mod_products_pharmacy_stock`
  ADD PRIMARY KEY (`mod_prod_pha_stk_id`) USING BTREE;

--
-- Indices de la tabla `mod_products_prices_list`
--
ALTER TABLE `mod_products_prices_list`
  ADD PRIMARY KEY (`mod_prod_prl_id`,`mod_prod_prl_prod_id`) USING BTREE;

--
-- Indices de la tabla `mod_products_relation`
--
ALTER TABLE `mod_products_relation`
  ADD PRIMARY KEY (`mod_prod_rel_id`,`mod_prod_rel_prod_id`,`mod_prod_rel_ent_id`) USING BTREE;

--
-- Indices de la tabla `mod_products_types`
--
ALTER TABLE `mod_products_types`
  ADD PRIMARY KEY (`mod_prod_type_id`) USING BTREE;

--
-- Indices de la tabla `mod_products_warehouses`
--
ALTER TABLE `mod_products_warehouses`
  ADD PRIMARY KEY (`mod_prod_wrh_prod_id`,`mod_prod_wrh_wrh_id`) USING BTREE;

--
-- Indices de la tabla `mod_providers`
--
ALTER TABLE `mod_providers`
  ADD PRIMARY KEY (`mod_prv_id`) USING BTREE;

--
-- Indices de la tabla `mod_rehabilitation_centers`
--
ALTER TABLE `mod_rehabilitation_centers`
  ADD PRIMARY KEY (`mod_rhb_center_id`) USING BTREE;

--
-- Indices de la tabla `mod_rehabilitation_intern`
--
ALTER TABLE `mod_rehabilitation_intern`
  ADD PRIMARY KEY (`mod_rhb_int_id`,`mod_rhb_int_acu_id`) USING BTREE;

--
-- Indices de la tabla `mod_rehabilitation_primary_consumption`
--
ALTER TABLE `mod_rehabilitation_primary_consumption`
  ADD PRIMARY KEY (`mod_rhb_pcn_id`) USING BTREE;

--
-- Indices de la tabla `mod_rehabilitation_tutor`
--
ALTER TABLE `mod_rehabilitation_tutor`
  ADD PRIMARY KEY (`mod_rhb_tutor_id`) USING BTREE;

--
-- Indices de la tabla `mod_rehabilitation_user`
--
ALTER TABLE `mod_rehabilitation_user`
  ADD PRIMARY KEY (`mod_rhb_id`) USING BTREE;

--
-- Indices de la tabla `mod_rewards_register`
--
ALTER TABLE `mod_rewards_register`
  ADD PRIMARY KEY (`mod_rwd_reg_rwd_id`,`mod_rwd_reg_acu_id`,`mod_rwd_reg_cpa_id`) USING BTREE;

--
-- Indices de la tabla `mod_services`
--
ALTER TABLE `mod_services`
  ADD PRIMARY KEY (`mod_sv_id`) USING BTREE;

--
-- Indices de la tabla `mod_sliders`
--
ALTER TABLE `mod_sliders`
  ADD PRIMARY KEY (`mod_sli_id`) USING BTREE;

--
-- Indices de la tabla `mod_sliders_files`
--
ALTER TABLE `mod_sliders_files`
  ADD PRIMARY KEY (`mod_sli_file_file_id`,`mod_sli_file_sli_id`) USING BTREE;

--
-- Indices de la tabla `mod_sports`
--
ALTER TABLE `mod_sports`
  ADD PRIMARY KEY (`mod_spt_id`) USING BTREE;

--
-- Indices de la tabla `mod_sports_dates`
--
ALTER TABLE `mod_sports_dates`
  ADD PRIMARY KEY (`mod_sp_dt_id`) USING BTREE;

--
-- Indices de la tabla `mod_sports_games`
--
ALTER TABLE `mod_sports_games`
  ADD PRIMARY KEY (`mod_sp_gm_id`) USING BTREE;

--
-- Indices de la tabla `mod_sports_teams`
--
ALTER TABLE `mod_sports_teams`
  ADD PRIMARY KEY (`mod_sp_tm_id`) USING BTREE;

--
-- Indices de la tabla `mod_stock`
--
ALTER TABLE `mod_stock`
  ADD PRIMARY KEY (`mod_stk_id`) USING BTREE;

--
-- Indices de la tabla `mod_stock_receipt`
--
ALTER TABLE `mod_stock_receipt`
  ADD PRIMARY KEY (`mod_str_id`) USING BTREE;

--
-- Indices de la tabla `mod_syllabus`
--
ALTER TABLE `mod_syllabus`
  ADD PRIMARY KEY (`mod_syl_id`) USING BTREE;

--
-- Indices de la tabla `mod_syllabus_categorys`
--
ALTER TABLE `mod_syllabus_categorys`
  ADD PRIMARY KEY (`mod_syl_cat_syl_id`,`mod_syl_cat_cat_id`) USING BTREE;

--
-- Indices de la tabla `mod_syllabus_contents`
--
ALTER TABLE `mod_syllabus_contents`
  ADD PRIMARY KEY (`mod_syl_cont_id`) USING BTREE;

--
-- Indices de la tabla `mod_syllabus_periods`
--
ALTER TABLE `mod_syllabus_periods`
  ADD PRIMARY KEY (`mod_syl_per_id`) USING BTREE;

--
-- Indices de la tabla `mod_syllabus_relations_periods`
--
ALTER TABLE `mod_syllabus_relations_periods`
  ADD PRIMARY KEY (`mod_syl_rel_per_syl_id`,`mod_syl_rel_per_per_id`) USING BTREE;

--
-- Indices de la tabla `mod_warehouses`
--
ALTER TABLE `mod_warehouses`
  ADD PRIMARY KEY (`mod_wrh_id`) USING BTREE;

--
-- Indices de la tabla `notifications`
--
ALTER TABLE `notifications`
  ADD PRIMARY KEY (`ntf_id`) USING BTREE;

--
-- Indices de la tabla `options`
--
ALTER TABLE `options`
  ADD PRIMARY KEY (`option_id`) USING BTREE;

--
-- Indices de la tabla `posts`
--
ALTER TABLE `posts`
  ADD PRIMARY KEY (`post_id`) USING BTREE;

--
-- Indices de la tabla `posts_authors`
--
ALTER TABLE `posts_authors`
  ADD PRIMARY KEY (`post_au_id`) USING BTREE;

--
-- Indices de la tabla `posts_categorys`
--
ALTER TABLE `posts_categorys`
  ADD PRIMARY KEY (`post_cat_post_id`,`post_cat_cat_id`) USING BTREE;

--
-- Indices de la tabla `posts_files`
--
ALTER TABLE `posts_files`
  ADD PRIMARY KEY (`post_file_post_id`,`post_file_file_id`) USING BTREE;

--
-- Indices de la tabla `publications`
--
ALTER TABLE `publications`
  ADD PRIMARY KEY (`pub_id`) USING BTREE;

--
-- Indices de la tabla `publications_pattern`
--
ALTER TABLE `publications_pattern`
  ADD PRIMARY KEY (`pub_pat_id`) USING BTREE;

--
-- Indices de la tabla `publications_relations`
--
ALTER TABLE `publications_relations`
  ADD PRIMARY KEY (`pub_rel_id`) USING BTREE;

--
-- Indices de la tabla `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`rol_id`) USING BTREE;

--
-- Indices de la tabla `roles_categorys`
--
ALTER TABLE `roles_categorys`
  ADD PRIMARY KEY (`rol_cat_rol_id`,`rol_cat_cat_id`) USING BTREE;

--
-- Indices de la tabla `roles_entities`
--
ALTER TABLE `roles_entities`
  ADD PRIMARY KEY (`rol_ent_rol_id`,`rol_ent_ent_id`) USING BTREE;

--
-- Indices de la tabla `roles_modules`
--
ALTER TABLE `roles_modules`
  ADD PRIMARY KEY (`rol_mod_mod_id`,`rol_mod_rol_id`) USING BTREE;

--
-- Indices de la tabla `roles_sites`
--
ALTER TABLE `roles_sites`
  ADD PRIMARY KEY (`rol_site_rol_id`,`rol_site_site_id`) USING BTREE;

--
-- Indices de la tabla `roles_systems`
--
ALTER TABLE `roles_systems`
  ADD PRIMARY KEY (`rol_sys_rol_id`,`rol_sys_sys_id`,`rol_sys_ent_id`) USING BTREE;

--
-- Indices de la tabla `sites`
--
ALTER TABLE `sites`
  ADD PRIMARY KEY (`site_id`) USING BTREE;

--
-- Indices de la tabla `sites_entities`
--
ALTER TABLE `sites_entities`
  ADD PRIMARY KEY (`site_ent_site_id`,`site_ent_ent_id`) USING BTREE;

--
-- Indices de la tabla `systems`
--
ALTER TABLE `systems`
  ADD PRIMARY KEY (`sys_id`) USING BTREE;

--
-- Indices de la tabla `systems_modules`
--
ALTER TABLE `systems_modules`
  ADD PRIMARY KEY (`sys_mod_sys_id`,`sys_mod_mod_id`,`sys_mod_ent_id`) USING BTREE;

--
-- Indices de la tabla `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`user_id`) USING BTREE;

--
-- Indices de la tabla `users_firms`
--
ALTER TABLE `users_firms`
  ADD PRIMARY KEY (`user_firm_id`,`user_firm_user_id`) USING BTREE;

--
-- Indices de la tabla `users_groups`
--
ALTER TABLE `users_groups`
  ADD PRIMARY KEY (`user_group_user_id`,`user_group_group_id`) USING BTREE;

--
-- Indices de la tabla `users_path`
--
ALTER TABLE `users_path`
  ADD PRIMARY KEY (`user_path_user_id`,`user_path_mod_id`) USING BTREE;

--
-- Indices de la tabla `users_roles`
--
ALTER TABLE `users_roles`
  ADD PRIMARY KEY (`user_rol_user_id`,`user_rol_rol_id`) USING BTREE;

--
-- Indices de la tabla `users_tabs`
--
ALTER TABLE `users_tabs`
  ADD PRIMARY KEY (`user_tab_user_id`,`user_tab_sys_id`,`user_tab_ent_id`) USING BTREE;

--
-- Indices de la tabla `users_tokens`
--
ALTER TABLE `users_tokens`
  ADD PRIMARY KEY (`user_tk_type`,`user_tk_token`) USING BTREE;

--
-- Indices de la tabla `worksheets`
--
ALTER TABLE `worksheets`
  ADD PRIMARY KEY (`ws_id`) USING BTREE;

--
-- Indices de la tabla `worksheets_blocks`
--
ALTER TABLE `worksheets_blocks`
  ADD PRIMARY KEY (`ws_block_ws_id`,`ws_block_block_id`) USING BTREE;

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `blocks`
--
ALTER TABLE `blocks`
  MODIFY `block_id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `categorys`
--
ALTER TABLE `categorys`
  MODIFY `cat_id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT de la tabla `contents`
--
ALTER TABLE `contents`
  MODIFY `cont_id` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `docs`
--
ALTER TABLE `docs`
  MODIFY `doc_id` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `entities`
--
ALTER TABLE `entities`
  MODIFY `ent_id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `files`
--
ALTER TABLE `files`
  MODIFY `file_id` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `groups`
--
ALTER TABLE `groups`
  MODIFY `group_id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `links`
--
ALTER TABLE `links`
  MODIFY `lnk_id` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `menus`
--
ALTER TABLE `menus`
  MODIFY `menu_id` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `menus_items`
--
ALTER TABLE `menus_items`
  MODIFY `menu_item_id` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `modules`
--
ALTER TABLE `modules`
  MODIFY `mod_id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2104;

--
-- AUTO_INCREMENT de la tabla `mod_accounting_plan`
--
ALTER TABLE `mod_accounting_plan`
  MODIFY `mod_acp_id` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `mod_accounts_ads`
--
ALTER TABLE `mod_accounts_ads`
  MODIFY `mod_aca_id` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `mod_accounts_invoices_data`
--
ALTER TABLE `mod_accounts_invoices_data`
  MODIFY `mod_acu_invd_id` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `mod_accounts_roles`
--
ALTER TABLE `mod_accounts_roles`
  MODIFY `mod_acu_rol_id` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `mod_accounts_users`
--
ALTER TABLE `mod_accounts_users`
  MODIFY `mod_acu_id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=47;

--
-- AUTO_INCREMENT de la tabla `mod_actions`
--
ALTER TABLE `mod_actions`
  MODIFY `mod_act_id` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `mod_acuse`
--
ALTER TABLE `mod_acuse`
  MODIFY `mod_ac_id` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `mod_addresses`
--
ALTER TABLE `mod_addresses`
  MODIFY `mod_add_id` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `mod_ads`
--
ALTER TABLE `mod_ads`
  MODIFY `mod_ads_id` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `mod_ads_customers`
--
ALTER TABLE `mod_ads_customers`
  MODIFY `mod_ads_cus_id` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `mod_ads_position`
--
ALTER TABLE `mod_ads_position`
  MODIFY `mod_ads_pos_id` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `mod_advised_calendars`
--
ALTER TABLE `mod_advised_calendars`
  MODIFY `mod_adv_cal_id` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `mod_advised_calendars_blocked`
--
ALTER TABLE `mod_advised_calendars_blocked`
  MODIFY `mod_adv_cb_id` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `mod_advised_counselors_fields`
--
ALTER TABLE `mod_advised_counselors_fields`
  MODIFY `mod_adv_fds_user_id` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `mod_advised_history`
--
ALTER TABLE `mod_advised_history`
  MODIFY `mod_adv_his_id` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `mod_advised_options`
--
ALTER TABLE `mod_advised_options`
  MODIFY `mod_adv_opt_id` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `mod_advised_schedules`
--
ALTER TABLE `mod_advised_schedules`
  MODIFY `mod_adv_sch_id` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `mod_advised_types_appointments`
--
ALTER TABLE `mod_advised_types_appointments`
  MODIFY `mod_adv_tya_id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `mod_advised_user`
--
ALTER TABLE `mod_advised_user`
  MODIFY `mod_adv_id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=46;

--
-- AUTO_INCREMENT de la tabla `mod_brands`
--
ALTER TABLE `mod_brands`
  MODIFY `mod_brd_id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `mod_campaigns_ads`
--
ALTER TABLE `mod_campaigns_ads`
  MODIFY `mod_cpa_id` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `mod_carts`
--
ALTER TABLE `mod_carts`
  MODIFY `mod_cart_id` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `mod_classification_info`
--
ALTER TABLE `mod_classification_info`
  MODIFY `mod_clsi_id` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `mod_competitions`
--
ALTER TABLE `mod_competitions`
  MODIFY `mod_cmp_id` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `mod_competitions_mira`
--
ALTER TABLE `mod_competitions_mira`
  MODIFY `mod_cmp_mira_id` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `mod_customers_data`
--
ALTER TABLE `mod_customers_data`
  MODIFY `mod_ced_id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT de la tabla `mod_customers_enterprises`
--
ALTER TABLE `mod_customers_enterprises`
  MODIFY `mod_cen_id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT de la tabla `mod_customers_persons`
--
ALTER TABLE `mod_customers_persons`
  MODIFY `mod_cpe_id` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `mod_departments`
--
ALTER TABLE `mod_departments`
  MODIFY `mod_dep_id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT de la tabla `mod_discount`
--
ALTER TABLE `mod_discount`
  MODIFY `mod_dis_id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `mod_events`
--
ALTER TABLE `mod_events`
  MODIFY `mod_eve_id` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `mod_events_halls`
--
ALTER TABLE `mod_events_halls`
  MODIFY `mod_eve_hall_id` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `mod_events_options`
--
ALTER TABLE `mod_events_options`
  MODIFY `mod_eve_option_id` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `mod_events_tickets`
--
ALTER TABLE `mod_events_tickets`
  MODIFY `mod_eve_tck_id` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `mod_external_docs`
--
ALTER TABLE `mod_external_docs`
  MODIFY `mod_edc_id` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `mod_external_docs_options`
--
ALTER TABLE `mod_external_docs_options`
  MODIFY `mod_edc_opt_id` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `mod_external_docs_page_design`
--
ALTER TABLE `mod_external_docs_page_design`
  MODIFY `mod_edc_pd_id` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `mod_external_docs_templates`
--
ALTER TABLE `mod_external_docs_templates`
  MODIFY `mod_edc_tpl_id` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `mod_external_docs_types`
--
ALTER TABLE `mod_external_docs_types`
  MODIFY `mod_edc_type_id` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `mod_inventory_options`
--
ALTER TABLE `mod_inventory_options`
  MODIFY `mod_inv_opt_id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `mod_inventory_units`
--
ALTER TABLE `mod_inventory_units`
  MODIFY `mod_inv_unt_id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=125;

--
-- AUTO_INCREMENT de la tabla `mod_jobtitle`
--
ALTER TABLE `mod_jobtitle`
  MODIFY `mod_jbt_id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=33;

--
-- AUTO_INCREMENT de la tabla `mod_kardex`
--
ALTER TABLE `mod_kardex`
  MODIFY `mod_kdx_id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10012;

--
-- AUTO_INCREMENT de la tabla `mod_leagues`
--
ALTER TABLE `mod_leagues`
  MODIFY `mod_lg_id` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `mod_leagues_divisions`
--
ALTER TABLE `mod_leagues_divisions`
  MODIFY `mod_lg_div_id` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `mod_logistics_distribution_move`
--
ALTER TABLE `mod_logistics_distribution_move`
  MODIFY `mod_log_dtz_id` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `mod_logistics_distribution_zone`
--
ALTER TABLE `mod_logistics_distribution_zone`
  MODIFY `mod_log_dtz_id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `mod_make`
--
ALTER TABLE `mod_make`
  MODIFY `mod_mak_id` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `mod_organization_chart`
--
ALTER TABLE `mod_organization_chart`
  MODIFY `mod_orc_id` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `mod_payments_tx_tickets`
--
ALTER TABLE `mod_payments_tx_tickets`
  MODIFY `mod_pay_tx_tck_id` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `mod_places`
--
ALTER TABLE `mod_places`
  MODIFY `mod_plc_id` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `mod_prices_list`
--
ALTER TABLE `mod_prices_list`
  MODIFY `mod_prl_id` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `mod_producers`
--
ALTER TABLE `mod_producers`
  MODIFY `mod_prd_id` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `mod_products`
--
ALTER TABLE `mod_products`
  MODIFY `mod_prod_id` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `mod_products_clothing_store`
--
ALTER TABLE `mod_products_clothing_store`
  MODIFY `mod_prod_cst_id` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `mod_products_colors`
--
ALTER TABLE `mod_products_colors`
  MODIFY `mod_prod_color_id` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `mod_products_food`
--
ALTER TABLE `mod_products_food`
  MODIFY `mod_prod_food_id` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `mod_products_options`
--
ALTER TABLE `mod_products_options`
  MODIFY `mod_prod_opt_id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT de la tabla `mod_products_pharmacy`
--
ALTER TABLE `mod_products_pharmacy`
  MODIFY `mod_prod_pha_id` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `mod_products_pharmacy_stock`
--
ALTER TABLE `mod_products_pharmacy_stock`
  MODIFY `mod_prod_pha_stk_id` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `mod_products_prices_list`
--
ALTER TABLE `mod_products_prices_list`
  MODIFY `mod_prod_prl_id` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `mod_products_relation`
--
ALTER TABLE `mod_products_relation`
  MODIFY `mod_prod_rel_id` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `mod_products_types`
--
ALTER TABLE `mod_products_types`
  MODIFY `mod_prod_type_id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `mod_providers`
--
ALTER TABLE `mod_providers`
  MODIFY `mod_prv_id` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `mod_rehabilitation_centers`
--
ALTER TABLE `mod_rehabilitation_centers`
  MODIFY `mod_rhb_center_id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `mod_rehabilitation_intern`
--
ALTER TABLE `mod_rehabilitation_intern`
  MODIFY `mod_rhb_int_id` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `mod_rehabilitation_primary_consumption`
--
ALTER TABLE `mod_rehabilitation_primary_consumption`
  MODIFY `mod_rhb_pcn_id` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `mod_rehabilitation_tutor`
--
ALTER TABLE `mod_rehabilitation_tutor`
  MODIFY `mod_rhb_tutor_id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `mod_rehabilitation_user`
--
ALTER TABLE `mod_rehabilitation_user`
  MODIFY `mod_rhb_id` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `mod_services`
--
ALTER TABLE `mod_services`
  MODIFY `mod_sv_id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `mod_sliders`
--
ALTER TABLE `mod_sliders`
  MODIFY `mod_sli_id` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `mod_sports`
--
ALTER TABLE `mod_sports`
  MODIFY `mod_spt_id` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `mod_sports_dates`
--
ALTER TABLE `mod_sports_dates`
  MODIFY `mod_sp_dt_id` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `mod_sports_games`
--
ALTER TABLE `mod_sports_games`
  MODIFY `mod_sp_gm_id` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `mod_sports_teams`
--
ALTER TABLE `mod_sports_teams`
  MODIFY `mod_sp_tm_id` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `mod_stock`
--
ALTER TABLE `mod_stock`
  MODIFY `mod_stk_id` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `mod_stock_receipt`
--
ALTER TABLE `mod_stock_receipt`
  MODIFY `mod_str_id` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `mod_syllabus`
--
ALTER TABLE `mod_syllabus`
  MODIFY `mod_syl_id` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `mod_syllabus_contents`
--
ALTER TABLE `mod_syllabus_contents`
  MODIFY `mod_syl_cont_id` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `mod_syllabus_periods`
--
ALTER TABLE `mod_syllabus_periods`
  MODIFY `mod_syl_per_id` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `mod_warehouses`
--
ALTER TABLE `mod_warehouses`
  MODIFY `mod_wrh_id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `notifications`
--
ALTER TABLE `notifications`
  MODIFY `ntf_id` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `options`
--
ALTER TABLE `options`
  MODIFY `option_id` bigint UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT de la tabla `posts`
--
ALTER TABLE `posts`
  MODIFY `post_id` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `posts_authors`
--
ALTER TABLE `posts_authors`
  MODIFY `post_au_id` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `publications`
--
ALTER TABLE `publications`
  MODIFY `pub_id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT de la tabla `publications_pattern`
--
ALTER TABLE `publications_pattern`
  MODIFY `pub_pat_id` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `publications_relations`
--
ALTER TABLE `publications_relations`
  MODIFY `pub_rel_id` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `roles`
--
ALTER TABLE `roles`
  MODIFY `rol_id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT de la tabla `sites`
--
ALTER TABLE `sites`
  MODIFY `site_id` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `systems`
--
ALTER TABLE `systems`
  MODIFY `sys_id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- AUTO_INCREMENT de la tabla `users`
--
ALTER TABLE `users`
  MODIFY `user_id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `users_firms`
--
ALTER TABLE `users_firms`
  MODIFY `user_firm_id` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `worksheets`
--
ALTER TABLE `worksheets`
  MODIFY `ws_id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
