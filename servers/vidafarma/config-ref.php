<?php
header('Content-Type: text/html; charset=utf8');
error_reporting(E_ALL & ~E_NOTICE & ~E_DEPRECATED & ~E_WARNING);
ini_set('display_errors', 'On');

// Vars
$print = "off";
$folder = "ion/";
// DataBase
define('_HOST', 'localhost');
define('_DATA_BASE', 'ion_base');
define('_USER', 'dev');
define('_PASSWORD', 'qwe123AS!@#');
define('_PORT', '3306');
define('_CHARSET', 'utf8');

// PathPhysical Server
define('_PATH_SERVER', $_SERVER['DOCUMENT_ROOT'] . "/");
define('_PATH_NUCLEO', _PATH_SERVER . "nucleo/nucleo.3/");
define('_PATH_HOST', _PATH_SERVER . $folder);
define('_PATH_HOST_FILES', _PATH_SERVER . $folder);
define('_TYPE_CONFIG', 'htaccess'); //htaccess - webconfig
define('_TYPE_URI', 'https://'); //http:// , https://

// PathLogic
define('_PATH_WEB', _TYPE_URI . $_SERVER['HTTP_HOST'] . "/" . $folder);
define("_PATH_WEB_NUCLEO", _TYPE_URI . $_SERVER['HTTP_HOST'] . "/nucleo/nucleo.3/");
define("_PATH_FILES", _TYPE_URI . $_SERVER['HTTP_HOST'] . "/" . $folder);
define("_PATH_IMAGES", "");
define("_PATH_DOCS", "");
define("_PATH_PAGE", $folder);
define("_STATE_CONNECT", 0);
// ConfigPath
define("_PATH_LOGOUT", _PATH_WEB);
define("_ID_ENTITIE", "1");

// MailPrincipal
define('_EMAIL_SMTP', 'mail.wappcom.com');
define('_EMAIL_USER', 'mail@wappcom.com.bo');
define('_EMAIL_PASSWORD', 'Wappcom123AS!');
define('_EMAIL_PORT', 465);
define('_EMAIL_SEGURITY', 'ssl');

//Social
define("_KEY_GOOGLE", "");

// Browser
$langArray = explode(",", $_SERVER['HTTP_ACCEPT_LANGUAGE']);
define("_LANG", $langArray[0]);

// Data Nucleo
define('_V', "Ion 1.0 Local");
define('_VF', "@2020 Nucleo 3+");
//define('_ICONS_LINK', "https://wappcom.com/proy/font/font-brave/src/css/style.css");
define('_ICONS_LINK_FILES', 'https://wappcom.com/proy/font/font-brave/src/css/style.css');
define('_ICONS_LINK', _TYPE_URI . $_SERVER['HTTP_HOST'] . "/fonts/font-brave/src/style.css");
define("_PATH_CONSTRUCTOR", _PATH_NUCLEO . "models/class/class.constructor.php");

// Print
if ($print == "on") {
    $html = '';
    $html .= '<b>DataBase</b></br>';
    $html .= '_HOST: ' . _HOST . '</br></br>';
    $html .= '<b>Server</b></br>';
    $html .= '_PATH_SERVER: ' . _PATH_SERVER . '</br>';
    $html .= '_PATH_NUCLEO: ' . _PATH_NUCLEO . '</br>';
    $html .= '_PATH_HOST: ' . _PATH_HOST . '</br>';
    $html .= '_PATH_CONSTRUCTOR: ' . _PATH_CONSTRUCTOR . '</br>';
    $html .= '_TYPE_CONFIG: ' . _TYPE_CONFIG . '</br>';
    $html .= '_TYPE_URI: ' . _TYPE_URI . '</br></br>';
    $html .= '<b>PathLogic</b></br>';
    $html .= '_PATH_WEB: ' . _PATH_WEB . '</br>';
    $html .= '_PATH_WEB_NUCLEO: ' . _PATH_WEB_NUCLEO . '</br>';
    $html .= '_PATH_FILES: ' . _PATH_FILES . '</br>';
    $html .= '_PATH_PAGE: ' . _PATH_PAGE . '</br></br>';
    $html .= '<b>ConfigPath</b></br>';
    $html .= '_PATH_LOGOUT: ' . _PATH_LOGOUT . '</br></br>';
    $html .= '<b>MailPrincipal</b></br>';
    $html .= '_EMAIL_SMTP: ' . _EMAIL_SMTP . '</br>';
    $html .= '_EMAIL_USER: ' . _EMAIL_USER . '</br>';
    $html .= '_EMAIL_PORT: ' . _EMAIL_PORT . '</br>';
    $html .= '_EMAIL_SEGURITY: ' . _EMAIL_SEGURITY . '</br></br>';
    echo $html;
}